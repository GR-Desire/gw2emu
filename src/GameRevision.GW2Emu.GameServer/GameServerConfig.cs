﻿using System;
using GameRevision.GW2Emu.Common;

namespace GameRevision.GW2Emu.GameServer
{
    public class GameServerConfig
    {
        private static string FileName = "GameRevision.GW2Emu.GameServer.xml";

        public static GameServerConfig Load()
        {
            if (!XmlConfig.Exists(FileName))
            {
                XmlConfig.Save<GameServerConfig>(FileName, new GameServerConfig()
                    {
                        IP = "0.0.0.0",
                        Port = 9112,
                        RpcIP = "127.0.0.1",
                        RpcPort = 15471,
                        Id = 1001,
                        Type = 3 // 3 -> Game, 1 -> Character creation
                    });
            }

            return XmlConfig.Load<GameServerConfig>(FileName);
        }

        public string IP;
        public int Port;
        public string RpcIP;
        public int RpcPort;
        public int Id;
        public int Type;
    }
}
