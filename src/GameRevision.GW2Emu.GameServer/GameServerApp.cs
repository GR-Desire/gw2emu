using System;
using System.Net;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Network;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.GameServer.Session;
using System.Net.Sockets;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.GameServer.Controllers;
using System.Collections.Generic;
using System.Collections.Concurrent;
using log4net;

namespace GameRevision.GW2Emu.GameServer
{
    public class GameServerApp
    {
        private static ILog logger = LogManager.GetLogger(typeof(GameServerApp));

        private IEventAggregator eventAggregator;
        private IEventAggregator rpcEventAggregator;
        private GameServerConfig config;
        private ClientListener clientListener;
        private RpcSession rpcSession;
        private SessionManager sessionManager;

        private static GameServerApp instance;
        public static GameServerApp Instance
        {
            get
            {
                if (instance == null)
                    instance = new GameServerApp();
                return instance;
            }
        }

        public GameServerApp()
        {
            this.eventAggregator = new EventAggregator();
            this.rpcEventAggregator = new EventAggregator();

            this.sessionManager = new SessionManager();

            this.config = GameServerConfig.Load();

            IPAddress address;

            if (!IPAddress.TryParse(this.config.IP, out address))
            {
                address = IPAddress.Any;

                logger.Error("Couldn't parse IP address from config. Running on default.");
            }

            this.clientListener = new ClientListener(IPAddress.Any, this.config.Port);
            this.clientListener.ClientConnected += OnClientConnected;

            logger.InfoFormat("Server created on {0}:{1}.", this.clientListener.EndPoint.Address, this.clientListener.EndPoint.Port);

            RegisterLoginServer();

            RpcView.SendRegisterServer(rpcSession, this.config.Id, this.config.Type);
        }

        private void RegisterLoginServer()
        {
            this.rpcEventAggregator.Register(new RpcController());

            this.rpcEventAggregator.Trigger(new StartupEvent());

            rpcSession = new RpcSession(this.config.RpcIP, this.config.RpcPort, this.rpcEventAggregator);
        }

        public void RegisterHandlers()
        {
            this.eventAggregator.Register(new WorldEntryController());
            this.eventAggregator.Register(new CharacterCreationController());
            this.eventAggregator.Register(new CharacterController());
            this.eventAggregator.Register(new SessionController());
            this.eventAggregator.Register(new ChatController());

            this.eventAggregator.Trigger(new StartupEvent());

            logger.Info("Registered handlers.");
        }

        public void Start()
        {
            this.clientListener.Listen();
            this.rpcSession.Start();

            logger.Info("Server started.");
        }

        public void Stop()
        {
            this.clientListener.Stop();

            logger.Info("Server stopped.");
        }

        public void UpdateOneFrame(float delta)
        {
            this.sessionManager.Flush();
            this.sessionManager.Update(delta);
        }

        private void OnClientConnected(object sender, ClientConnectedEventArgs e)
        {
            GenericSession session = new GameSession(e.Client, this.eventAggregator);
            this.sessionManager.Add(session);
            session.Start();
        }

        public void RemoveSession(ISession session)
        {
            this.sessionManager.Remove(session);
        }
    }
}

