﻿using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.GameServer.Session;
using GameRevision.GW2Emu.GameServer.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.GameServer.Models;
using GameRevision.GW2Emu.Common.Data;

namespace GameRevision.GW2Emu.GameServer.Service
{
    public class MasterChatService
    {
        private ChatService worldChatService;
        private Guid chatId;

        public MasterChatService()
        {
            this.worldChatService = new ChatService();
            this.chatId = Guid.NewGuid();
        }

        #region Session management

        public void Add(ISession session)
        {
            this.worldChatService.Add(session);
        }

        public void Remove(ISession session)
        {
            this.worldChatService.Remove(session);
        }
        #endregion

        public void ProcessMessage(ISession sender, string message, byte type)
        {
            GameSessionAttachment attachment = sender.Attachment as GameSessionAttachment;

            switch (type)
            {
                default:
                    this.worldChatService.SendUserMessage(message, type, attachment.GameSession.CharacterName, attachment.Id);
                    break;
            }
        }

        public void Announce(string message)
        {
            this.worldChatService.SendUserMessage(message, ChatType.kWorld, "SERVER", this.chatId);
        }
    }
}