﻿using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.GameServer.Models;
using GameRevision.GW2Emu.GameServer.Views;

namespace GameRevision.GW2Emu.GameServer.Service
{
    public class ChatService
    {
        protected SessionManager sessionManager;

        public ChatService()
        {
            this.sessionManager = new SessionManager();
        }

        public void SendUserMessage(string message, byte type, string username, Guid senderId)
        {
            this.sessionManager.Iterate(s =>
            {
                ChatModel.ProcessChatMessage(s, message, type, username, senderId);
            });
        }

        public void Add(ISession session)
        {
            this.sessionManager.AddFlush(session);
        }

        public void Remove(ISession session)
        {
            this.sessionManager.RemoveFlush(session);
        }
    }
}