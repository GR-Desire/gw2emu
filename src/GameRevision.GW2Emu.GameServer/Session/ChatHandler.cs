﻿using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.GameServer.Models;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.GameServer.World;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Math;

namespace GameRevision.GW2Emu.GameServer.Session
{
    public static class ChatHandler
    {
        public static void HandleCommand(ISession session, string message)
        {
            var t = typeof(ChatHandler);
            var s = message.Substring(1);
            s = s.Split(' ')[0];
            var m = t.GetMethod(s, BindingFlags.IgnoreCase | BindingFlags.Static | BindingFlags.Public | BindingFlags.Instance);
            if (m != null && m.Name != "HandleCommand")
            {
                m.Invoke(null, new object[] { session, s.Split(' ') });
            }
            else
                ChatModel.ServerMessage(session, "This command does not exist");
        }

        public static void Users(ISession session, string[] parameters)
        {
            ChatModel.ServerMessage(session, World.World.Instance.PlayerCount + " player(s) are online.");
        }

        public static void Help(ISession session, string[] parameters)
        {
            ChatModel.ServerMessage(session, ".users : Display the number of players currently logged in.");
            ChatModel.ServerMessage(session, ".help : Display this message.");
            ChatModel.ServerMessage(session, ".npcspawn : Spawn a NPC at your location.");
            ChatModel.ServerMessage(session, ".pos : Show your world position.");
        }

        public static void NpcSpawn(ISession session, string[] parameters)
        {
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            Map map = World.World.Instance.GetMap(attachment.GameSession.Character.Map);
            Agent agent = map.SpawnAgent();
            WorldPosition pos = attachment.GameSession.Character.Position.ConvertToWorldPosition();

            int id = 16983;
            short unk = 8189;
            if (parameters.Length > 2)
            {
                id = int.Parse(parameters[2]);
                if (parameters.Length > 3)
                {
                    unk = short.Parse(parameters[3]);
                }
            }

            AgentView.SendCreate(session, agent.Id, id, false, 536876213, 0, 50, pos, unk, new float[] { 240f, 2.0944f, 108f });

            session.Send(new StoC.P161_UnknownMessage()
            {
                AgentId = agent.Id,
                Name = "",
                Unknown2 = 3,
                Unknown7 = new Optional<StoC.P161_UnknownMessage.Struct3>(new StoC.P161_UnknownMessage.Struct3()
                {
                    Unknown4 = 0,
                    ModelId = 5911404414211,
                    ModelVariant = 2305843009823295991
                }),
                Unknown13 = new Optional<StoC.P161_UnknownMessage.Struct8>()
            });


            AgentView.SendPlayerInfo289(session, agent.Id, 0, 200);
            AgentView.SendAttitude(session, agent.Id, 0);
            AgentView.SendCoreStats(session, agent.Id, new byte[45], 1, 1, 9);
            AgentView.SendDeathInfo(session, agent.Id, false);
            AgentView.SendWeaponsOut(session, agent.Id, false);
            AgentView.SendHealth(session, agent.Id, false, 96, 96, 0.1f);
            AgentView.SendRegisterInventory(session, agent.Id);
            AgentView.SendWearingTownClothes(session, agent.Id, false);
            AgentView.SendInventory136(session, agent.Id, 0);
            AgentView.SendArmorVisibility(session, agent.Id, new int[] { 0, 0 });
            AgentView.SendInventory119(session, agent.Id);
            AgentView.SendAttunement(session, agent.Id, 0);
            AgentView.SendFinalize(session, agent.Id);
            AgentView.SendSpawn(session, agent.Id, 16983, 0, 2, 0, pos, 8189);

            ChatModel.ServerMessage(session, "NPC spawned at your location.");
        }

        public static void Pos(ISession session, string[] parameters)
        {
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            Map map = World.World.Instance.GetMap(attachment.GameSession.Character.Map);
            Agent agent = map.SpawnAgent();

            WorldPosition pos = attachment.GameSession.Character.Position.ConvertToWorldPosition();

            ChatModel.ServerMessage(session, "Your current position is : (X: " + pos.Vector.X + ", Y: " + pos.Vector.Y + ", Z: " + pos.Vector.Z + ")");
        }

        public static void Suicide(ISession session, string[] parameters)
        {
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

        }
    }
}