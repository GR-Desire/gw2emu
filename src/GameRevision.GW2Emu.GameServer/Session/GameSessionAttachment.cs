﻿using GameRevision.GW2Emu.GameServer.World;
using System;

namespace GameRevision.GW2Emu.GameServer.Session
{
    public class GameSessionAttachment
    {
        public class InGameSession
        {
            public Guid CharacterId;
            public Agent Character;
            public string CharacterName;
        }

        private InGameSession gameSession = new InGameSession();

        public string Mail { get; set; }
        public int Mode { get; set; }
        public Guid Id { get; set; }
        public InGameSession GameSession
        {
            get
            {
                return this.gameSession;
            }
            set
            {
                this.gameSession = value;
            }
        }
    }
}
