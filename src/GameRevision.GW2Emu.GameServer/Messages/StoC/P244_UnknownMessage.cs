/*
 * This code was autogenerated by
 * GameRevision.GW2Emu.CodeWriter.
 * Date generated: 21-09-13
 */

using System;
using System.IO;
using System.Net;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Messaging;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.GameServer.Messages.StoC
{
    public class P244_UnknownMessage : GenericMessage
    {
        public int Unknown0;
        public byte Unknown1;
        public int Unknown2;
        public struct Struct3
        {
            public int Unknown4;
            public int Unknown5;
            
            public void Serialize(Serializer serializer)
            {
                serializer.WriteVarint(this.Unknown4);
                serializer.WriteVarint(this.Unknown5);
            }
        }
        public Struct3[] Unknown6;
        public struct Struct7
        {
            public int Unknown8;
            public byte Unknown9;
            
            public void Serialize(Serializer serializer)
            {
                serializer.WriteVarint(this.Unknown8);
                serializer.Write(this.Unknown9);
            }
        }
        public Struct7[] Unknown10;
        public Optional<int> Unknown11;
        public Optional<int> Unknown12;
        public Optional<byte> Unknown13;
        public Optional<byte> Unknown14;
        public Optional<int> Unknown15;
        public Optional<byte> Unknown16;
        public struct Struct17
        {
            public Guid Unknown18;
            public byte Unknown19;
            public int Unknown20;
            public int Unknown21;
            public long Unknown22;
            
            public void Serialize(Serializer serializer)
            {
                serializer.Write(this.Unknown18);
                serializer.Write(this.Unknown19);
                serializer.WriteVarint(this.Unknown20);
                serializer.WriteVarint(this.Unknown21);
                serializer.Write(this.Unknown22);
            }
        }
        public Optional<Struct17> Unknown23;
        public Optional<int> Unknown24;
        public Optional<int> Unknown25;
        
        public override ushort Header
        {
            get
            {
                return 244;
            }
        }
        
        public override void Serialize(Serializer serializer)
        {
            serializer.Write(Header);
            serializer.WriteVarint(this.Unknown0);
            serializer.Write(this.Unknown1);
            serializer.WriteVarint(this.Unknown2);
            serializer.Write((byte)Unknown6.Length);
            for (int i = 0; i < Unknown6.Length; i++)
            {
                Unknown6[i].Serialize(serializer);
            }
            serializer.Write((byte)Unknown10.Length);
            for (int i = 0; i < Unknown10.Length; i++)
            {
                Unknown10[i].Serialize(serializer);
            }
            serializer.Write(this.Unknown11.IsPresent);
            if (this.Unknown11.IsPresent)
            {
                serializer.WriteVarint(this.Unknown11.Value);
            }
            serializer.Write(this.Unknown12.IsPresent);
            if (this.Unknown12.IsPresent)
            {
                serializer.WriteVarint(this.Unknown12.Value);
            }
            serializer.Write(this.Unknown13.IsPresent);
            if (this.Unknown13.IsPresent)
            {
                serializer.Write(this.Unknown13.Value);
            }
            serializer.Write(this.Unknown14.IsPresent);
            if (this.Unknown14.IsPresent)
            {
                serializer.Write(this.Unknown14.Value);
            }
            serializer.Write(this.Unknown15.IsPresent);
            if (this.Unknown15.IsPresent)
            {
                serializer.WriteVarint(this.Unknown15.Value);
            }
            serializer.Write(this.Unknown16.IsPresent);
            if (this.Unknown16.IsPresent)
            {
                serializer.Write(this.Unknown16.Value);
            }
            serializer.Write(this.Unknown23.IsPresent);
            if (this.Unknown23.IsPresent)
            {
                this.Unknown23.Value.Serialize(serializer);
            }
            serializer.Write(this.Unknown24.IsPresent);
            if (this.Unknown24.IsPresent)
            {
                serializer.WriteVarint(this.Unknown24.Value);
            }
            serializer.Write(this.Unknown25.IsPresent);
            if (this.Unknown25.IsPresent)
            {
                serializer.WriteVarint(this.Unknown25.Value);
            }
        }
    }
}
