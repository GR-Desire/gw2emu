/*
 * This code was autogenerated by
 * GameRevision.GW2Emu.CodeWriter.
 * Date generated: 21-09-13
 */

using System;
using System.IO;
using System.Net;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Messaging;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.GameServer.Messages.StoC
{
    public class P416_InventoryRegisterItemMessage : GenericMessage
    {
        public int RegisterId;
        public int ItemId;
        public byte Unknown2;
        public int Count;
        
        public override ushort Header
        {
            get
            {
                return 416;
            }
        }
        
        public override void Serialize(Serializer serializer)
        {
            serializer.Write(Header);
            serializer.WriteVarint(this.RegisterId);
            serializer.WriteVarint(this.ItemId);
            serializer.Write(this.Unknown2);
            serializer.WriteVarint(this.Count);
        }
    }
}
