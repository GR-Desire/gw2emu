/*
 * This code was autogenerated by
 * GameRevision.GW2Emu.CodeWriter.
 * Date generated: 21-09-13
 */

using System;
using System.IO;
using System.Net;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Messaging;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.GameServer.Messages.CtoS
{
    public class P220_UnknownMessage : GenericMessage
    {
        public int Unknown0;
        public Guid Unknown1;
        public int Unknown2;
        public int Unknown3;
        public int Unknown4;
        
        public override ushort Header
        {
            get
            {
                return 220;
            }
        }
        
        public override void Deserialize(Deserializer deserializer)
        {
            this.Unknown0 = deserializer.ReadVarint();
            this.Unknown1 = deserializer.ReadGuid();
            this.Unknown2 = deserializer.ReadVarint();
            this.Unknown3 = deserializer.ReadVarint();
            this.Unknown4 = deserializer.ReadVarint();
        }
    }
}
