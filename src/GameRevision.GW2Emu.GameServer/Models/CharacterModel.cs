﻿using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Database.DataEntities;
using GameRevision.GW2Emu.Common.Serialization;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.GameServer.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.GameServer.Models
{
    public static class CharacterModel
    {
        public static void SendCharacterCreated(ISession session, character character)
        {
            CharacterCreationView.Send588(session);
            CharacterCreationView.Send309(session);

            using (Serializer ser = new Serializer())
            {
                CharacterData.Serialize(ser, character);

                CharacterCreationView.SendCharacterInfo(session, character.id, character.account, character.name, 458, ser.GetBytes());
            }
        }
    }
}
