﻿using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.GameServer.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.GameServer.Session;
using GameRevision.GW2Emu.Common.Data;

namespace GameRevision.GW2Emu.GameServer.Models
{
    public static class ChatModel
    {
        public static void ProcessChatMessage(ISession session, string message, byte type, string name, Guid id)
        {
            ChatView.SendMessage(session, message.ToWordBit());
            ChatView.SendMessageOptions(session, type, 19, name, id);
        }

        public static void AnnounceMessage(ISession session, string message)
        {
            ProcessChatMessage(session, message, ChatType.kWorld, "SERVER", new Guid("e0cce457-0867-4375-a3a7-fcbb095b406c"));
        }

        public static void ServerMessage(ISession session, string message)
        {
            ProcessChatMessage(session, message, ChatType.kSay, "SERVER", new Guid("e0cce457-0867-4375-a3a7-fcbb095b406c"));
        }
    }
}