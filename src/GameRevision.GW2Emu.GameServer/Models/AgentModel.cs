﻿using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using GameRevision.GW2Emu.GameServer.World;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Data;

namespace GameRevision.GW2Emu.GameServer.Models
{
    public static class AgentModel
    {
        public static void SendAgent(ISession session, Agent agent)
        {
            AgentView.SendCreate(session, agent.Id, 16983, agent.IsMonster, agent.SpeciesDefinition, 0, agent.Party, agent.Position.ConvertToWorldPosition(), 8189, new float[] { 240f, 2.0944f, 108f });

            session.Send(new StoC.P161_UnknownMessage()
            {
                AgentId = agent.Id,
                Name = agent.Name.ToWordBit(),
                Unknown2 = 3,
                Unknown7 = new Optional<StoC.P161_UnknownMessage.Struct3>(new StoC.P161_UnknownMessage.Struct3()
                {
                    Unknown4 = 0,
                    ModelId = 5911404414211,
                    ModelVariant = 2305843009823295991
                }),
                Unknown13 = new Optional<StoC.P161_UnknownMessage.Struct8>()
            });


            AgentView.SendPlayerInfo289(session, agent.Id, 0, 5);
            AgentView.SendAttitude(session, agent.Id, agent.Attitude);
            AgentView.SendCoreStats(session, agent.Id, agent.Appearance, agent.Level, agent.AdjustedLevel, agent.Class);
            AgentView.SendDeathInfo(session, agent.Id, agent.IsDead);
            AgentView.SendWeaponsOut(session, agent.Id, agent.IsWeaponDrawn);
            AgentView.SendHealth(session, agent.Id, agent.IsDead, agent.Health, agent.MaximumHealth, agent.HealthRegeneration);
            AgentView.SendRegisterInventory(session, agent.Id);

            // Inventory goes here

            AgentView.SendWearingTownClothes(session, agent.Id, agent.IsWearingTownClothes);
            AgentView.SendInventory136(session, agent.Id, 0);
            AgentView.SendArmorVisibility(session, agent.Id, new int[] { 0, 0 });
            AgentView.SendInventory119(session, agent.Id);
            AgentView.SendAttunement(session, agent.Id, agent.Attunement);
            AgentView.SendFinalize(session, agent.Id);
            AgentView.SendSpawn(session, agent.Id, 16983, 0, 2, 0, agent.Position.ConvertToWorldPosition(), 8189);

        }
    }
}
