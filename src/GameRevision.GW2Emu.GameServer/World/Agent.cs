﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.GameServer.World
{
    public class Agent
    {
        public short Id { get; set; }
        public ushort Map { get; set; }
        public BulletSharp.Vector4 Position { get; set; }

        public bool IsMonster { get; set; }
        public bool IsDead { get; set; }
        public bool IsWeaponDrawn { get; set; }
        public bool IsWearingTownClothes { get; set; }

        public float Health { get; set; }
        public float MaximumHealth { get; set; }
        public float HealthRegeneration { get; set; }
        public int SpeciesDefinition { get; set; }
        public byte Party { get; set; }
        public int Attitude { get; set; }
        public int Attunement { get; set; }
        public string Name { get; set; }
        public byte Level { get; set; }
        public byte AdjustedLevel { get; set; }
        public byte Class { get; set; }
        public byte[] Appearance { get; set; }

        public Agent(short id, ushort map)
        {
            this.Id = id;
            this.Map = map;
            this.Position = new BulletSharp.Vector4();
            this.Name = "";
            this.Appearance = new byte[45];
        }
    }
}
