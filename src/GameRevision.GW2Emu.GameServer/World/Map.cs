﻿using GameRevision.GW2Emu.Common.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.GameServer.World
{
    public class Map
    {
        public ushort Id { get; set; }
        public RangeContainer AgentIdManager { get; set; }

        public Map()
        {
            AgentIdManager = new RangeContainer(129);
        }

        public Agent SpawnAgent()
        {
            return new Agent((short)this.AgentIdManager.Acquire(), this.Id);
        }
    }
}
