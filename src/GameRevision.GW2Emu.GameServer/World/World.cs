﻿using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.GameServer.Service;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.Common.Data;

namespace GameRevision.GW2Emu.GameServer.World
{
    public class World
    {
        private static ILog logger = LogManager.GetLogger(typeof(World));

        #region singleton

        private static World _instance = new World();
        public static World Instance
        {
            get
            {
                return _instance;
            }
        }

        #endregion

        private SessionManager sessionManager;
        private MasterChatService chatService;
        private Dictionary<int, Map> maps;

        public int PlayerCount
        {
            get
            {
                return this.sessionManager.Count;
            }
        }

        private World()
        {
            logger.Info("Creating the World.");

            this.sessionManager = new SessionManager();

            this.chatService = new MasterChatService();

            this.maps = new Dictionary<int, Map>();
            this.maps.Add(0, new Map());
        }

        public void Enter(ISession session)
        {
            this.sessionManager.AddFlush(session);

            this.chatService.Add(session);
        }

        public void Leave(ISession session)
        {
            this.sessionManager.RemoveFlush(session);

            this.chatService.Remove(session);
        }

        public Map GetMap(ushort id)
        {
            return this.maps[0]; //TODO change this
        }

        public void HandleChatMessage(ISession session, string message, byte type)
        {
            if (type == ChatType.kSay)
            {
                //TODO: map management and send in a 2000 unit range
                this.chatService.ProcessMessage(session, message, type);
            }
            else
                this.chatService.ProcessMessage(session, message, type);
        }
    }
}
