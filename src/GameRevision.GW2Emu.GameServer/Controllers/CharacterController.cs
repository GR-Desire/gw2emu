﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Globalization;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Common;

using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.GameServer.Session;

namespace GameRevision.GW2Emu.GameServer.Controllers
{
    public class CharacterController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<CtoS.P009_AgentRotateMessage>(this.OnAgentRotate);
            aggregator.Register<CtoS.P013_AgentMoveMessage>(this.OnAgentMove);
            aggregator.Register<CtoS.P008_AgentMoveDirectionMessage>(this.OnAgentMoveDirection);
            aggregator.Register<CtoS.P004_AgentStopMessage>(this.OnAgentStop);
        }

#region Movement

        private void OnAgentRotate(CtoS.P009_AgentRotateMessage message)
        {

        }

        private void OnAgentMove(CtoS.P013_AgentMoveMessage message)
        {
            ISession session = message.Session;
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            BulletSharp.Vector4 pos = new BulletSharp.Vector4(message.Position.X, message.Position.Y, message.Position.Z, 0);

            attachment.GameSession.Character.Position = pos.ConvertToBigCoordinates();
        }

        private void OnAgentMoveDirection(CtoS.P008_AgentMoveDirectionMessage message)
        {
            ISession session = message.Session;
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            attachment.GameSession.Character.Position = message.Position.ConvertToBigCoordinates();

            session.Send(new StoC.P027_AgentMoveDirectionMessage()
            {
                AgentId = message.AgentId,
                Unknown1 = 10230994,
                Unknown2 = 3,
                Unknown3 = message.Unknown3,
                Unknown4 = new byte[] { 17, 01 },
                Position = message.Position,
                Unknown6 = message.Unknown6,
                Unknown7 = message.Unknown7,
                Unknown8 = message.Unknown8,
                Unknown9 = message.Unknown9,
                Unknown10 = message.Unknown10,
                Unknown11 = message.Unknown11,
                Unknown12 = message.Unknown12,
                Unknown13 = message.Unknown13,
                Unknown18 = new Common.Optional<StoC.P027_AgentMoveDirectionMessage.Struct14>()
            });
        }

        private void OnAgentStop(CtoS.P004_AgentStopMessage message)
        {
            ISession session = message.Session;

            session.Send(new StoC.P009_AgentStopMessage()
            {
                AgentId = message.AgentId,
                Unknown1 = 10230998,
                Unknown2 = 67,
                Unknown3 = message.Unknown3,
                Unknown4 = message.Unknown4,
                Unknown5 = message.Unknown5,
                Unknown6 = message.Unknown6,
                Unknown7 = message.Unknown7,
                Unknown8 = message.Unknown8
            });
        }

#endregion

    }
}
