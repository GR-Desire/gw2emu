﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Globalization;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Common;

using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.GameServer.Session;
using GameRevision.GW2Emu.GameServer.Models;

namespace GameRevision.GW2Emu.GameServer.Controllers
{
    public class CharacterCreationController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<CtoS.P147_CharacterCreate>(this.OnCharacterCreate);
        }

        private void OnCharacterCreate(CtoS.P147_CharacterCreate message)
        {
            ISession session = message.Session;
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            using (LoginEntities entities = new LoginEntities())
            {
                var account = entities.accounts.Find(attachment.Id);
                character c = new character();
                c.id = Guid.NewGuid();
                c.Parent = account;
                c.name = message.Name;
                c.unknown0 = 7;
                c.mapId = 0x32;
                c.mapCompletion = 0;
                c.level = 1;
                c.@class = message.Class;
                c.appearance = message.Appearance.Take(45).ToArray();

                account.Characters.Add(c);

                entities.SaveChanges();

                CharacterModel.SendCharacterCreated(session, c);
            }
        }

    }
}
