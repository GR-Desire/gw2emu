﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Globalization;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Common;

using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.GameServer.Models;
using GameRevision.GW2Emu.GameServer.Session;

namespace GameRevision.GW2Emu.GameServer.Controllers
{
    public class ChatController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<CtoS.P084_ChatMessage>(this.OnChat);
        }

        private void OnChat(CtoS.P084_ChatMessage message)
        {
            ISession session = message.Session;

            if (message.Message[0] == '.')
            {
                ChatHandler.HandleCommand(session, message.Message);
            }
            else
                World.World.Instance.HandleChatMessage(session, message.Message, message.Type);
        }
    }
}