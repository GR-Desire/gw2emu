﻿using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Messaging.Rpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.GameServer.Controllers
{
    public class RpcController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<P0001_CreateSessionMessage>(this.OnCreateSession);
        }

        private void OnCreateSession(P0001_CreateSessionMessage message)
        {
        }

    }
}
