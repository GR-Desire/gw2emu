﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Globalization;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Common;

using GameRevision.GW2Emu.Common.Data;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using GameRevision.GW2Emu.GameServer.Views;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.GameServer.Session;
using GameRevision.GW2Emu.GameServer.Models;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.GameServer.World;

namespace GameRevision.GW2Emu.GameServer.Controllers
{
    public class WorldEntryController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<CtoS.P151_QueryDynamicMessage>(this.OnMessage151);
            aggregator.Register<CtoS.P120_UnknownMessage>(this.OnMessage120);
            aggregator.Register<CtoS.P155_MailRedirectMessage>(this.OnRedirect);

            aggregator.Register<CtoS.P143_QueryMapMessage>(this.OnMessage143);
            aggregator.Register<CtoS.P146_UnknownMessage>(this.OnMessage146);
        }

        private void OnMessage120(CtoS.P120_UnknownMessage message)
        {
            ISession session = message.Session;
        }

        private void OnRedirect(CtoS.P155_MailRedirectMessage message)
        {
            ISession session = message.Session;
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            using (LoginEntities entities = new LoginEntities())
            {
                var account = (from a in entities.accounts
                               where a.mail == message.Mail
                               select a)
                               .Include(a => a.AccountSession)
                               .Include(a => a.Characters)
                               .SingleOrDefault();

                if (account == null)
                {
                    session.Disconnect();
                    return;
                }

                attachment.Mail = message.Mail;
                attachment.Id = account.id;

                switch (account.AccountSession.map)
                {
                    case 0:
                        MapView.SendCharacterCreationLoad(session);
                        break;
                    default:
                        attachment.GameSession.CharacterName = account.AccountSession.SessionCharacter.name;
                        attachment.GameSession.CharacterId = account.AccountSession.SessionCharacter.id;
                        attachment.GameSession.Character = World.World.Instance.GetMap((ushort)account.AccountSession.map).SpawnAgent();
                        MapView.SendEnterWorld(session, account.AccountSession.SessionCharacter.name, 7, (short)account.AccountSession.map, 1001, 1, 10000, new BulletSharp.Vector3(-18809.6f, 17367.5f, -1226.97f));
                        break;
                }
            }
        }

        private void OnMessage143(CtoS.P143_QueryMapMessage message)
        {
            ISession session = message.Session;

            MapView.SendMapInformation(session, new BulletSharp.Vector3(-18809.6f, 17367.5f, -1226.97f), 50);
        }

        private void OnMessage146(CtoS.P146_UnknownMessage message)
        {
            ISession session = message.Session;
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            World.World.Instance.Enter(session);

            session.Send(new StoC.P465_UnknownMessage());

            // For movement stuff
            session.Send(new StoC.P022_UnknownMessage()
            {
                Unknown0 = attachment.GameSession.Character.Id,
                Unknown1 = 418159,
                Unknown2 = 16
            });
        }

        
        private void OnMessage151(CtoS.P151_QueryDynamicMessage message)
        {
            ISession session = message.Session;
            GameSessionAttachment attachment = session.Attachment as GameSessionAttachment;

            using (LoginEntities logon = new LoginEntities())
            {
                var s = (from a in logon.accounts
                         where a.id == attachment.Id
                         select a)
                         .Include(a => a.AccountSession)
                         .Include(a => a.Characters)
                         .Single().AccountSession;

                var c = s.SessionCharacter;

                Map map = World.World.Instance.GetMap((ushort)s.map);

                short creatureId = 7;
                Agent agent = attachment.GameSession.Character;

                AgentView.SendLocalAgentId(session, agent.Id);

                CreatureView.SendCreateCreature(session, creatureId, c.name, c.id, 0);
                CreatureView.SendPvPGlory(session, creatureId, 23244);
                CreatureView.Send184(session, creatureId, 0);

                AgentView.SendCreate(session, agent.Id, 0, false, 805306375, 16, 49, new Common.Math.WorldPosition(new BulletSharp.Vector3(-18809.6f, 17367.5f, -1226.97f), 0), 3801, new float[] { 294f, 2.0944f, 70f });
                AgentView.SendAssignCreature(session, agent.Id, (byte)creatureId);
                AgentView.SendPlayerInfo289(session, agent.Id, 0, 14);
                AgentView.SendAttitude(session, agent.Id, 0);

                // ----------------------------------------------
                // Test 
                session.Send(new StoC.P718_UnknownMessage()
                {
                    Unknown0 = agent.Id,
                    Unknown1 = 385594220,
                    Unknown5 = new StoC.P718_UnknownMessage.Struct2[1]
                });
                // ----------------------------------------------
                AgentView.SendPlayerCoreStats(session, agent.Id, c.appearance, c.level, 80, c.@class, 4679547, new StoC.P072_AgentPlayerCoreStatsMessage.Struct6[9], 0.263737f, 1.0f, 0.11888f);
                AgentView.SendDeathInfo(session, agent.Id, false);
                AgentView.SendWeaponsOut(session, agent.Id, false);
                AgentView.SendDownedPenalty(session, agent.Id, 0);
                AgentView.SendEndurance(session, agent.Id, 100, 100, 100);
                AgentView.SendHealth(session, agent.Id, false, 10000.0f, 10000.0f, 1.0f);
                AgentView.SendRegisterInventory(session, agent.Id);
                AgentView.SendUnlockBags(session, agent.Id, 5);
                AgentView.Send126(session, agent.Id, 30);
                AgentView.SendInventorySlots(session, agent.Id, 160);

                int id = 1;
                foreach (var item in c.Items)
                {
                    InventoryView.SendItem(session, agent.Id, item, id);
                    ++id;
                }

                AgentView.SendGatherSlot(session, agent.Id, 36);
                AgentView.SendDefaultWeaponSet(session, agent.Id, 1, 1);
                AgentView.SendDefaultWeaponSet(session, agent.Id, 0, 4);
                AgentView.SendCurrentEquippedWeaponSet(session, agent.Id, 4);
                AgentView.SendWearingTownClothes(session, agent.Id, false);
                AgentView.SendInventory136(session, agent.Id, 0);
                AgentView.SendArmorVisibility(session, agent.Id, new int[] { 0, 8192 });
                AgentView.SendInventory119(session, agent.Id);
                AgentView.SendPetInformation(session, agent.Id, 6, 0, new int[0]);
                AgentView.SendRegisterPet(session, agent.Id, 0, "", 0, 0);

                session.Send(new StoC.P204_UnknownMessage()
                {
                    Unknown0 = agent.Id,
                    Unknown1 = 4f,
                    Unknown2 = 0f,
                    Unknown3 = 0f
                });

                AgentView.SendAttunement(session, agent.Id, 0);
                AgentView.SendRegisterSkillBar(session, agent.Id);

                session.Send(new StoC.P272_UnknownMessage()
                {
                    Unknown0 = agent.Id,
                    Unknown1 = 5
                });

                session.Send(new StoC.P269_UnknownMessage()
                {
                    Unknown0 = agent.Id
                });

                session.Send(new StoC.P173_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = 0,
                    Unknown2 = 0
                });

                session.Send(new StoC.P278_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = 0
                });

                session.Send(new StoC.P185_CreatureApplyGuildMessage()
                {
                    CreatureId = creatureId,
                    GuildId = c.id
                });

                session.Send(new StoC.P188_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = 0
                });

                session.Send(new StoC.P187_CreatureTitleMessage()
                {
                    CreatureId = creatureId,
                    TitleId = 0
                });

                session.Send(new StoC.P055_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = 2,
                    Unknown2 = 1226
                });

                // Probably, not necessary
                session.Send(new StoC.P716_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown4 = new StoC.P716_UnknownMessage.Struct1[1]
                });

                session.Send(new StoC.P057_CreatureAchievementPointsMessage()
                {
                    CreatureId = creatureId,
                    Unknown1 = 482,
                    Unknown2 = 226,
                    Unknown3 = 57
                });

                QuestView.SendPersonalStoryLine(session, creatureId, new short[0]);
                QuestView.SendQuestManager(session, creatureId);

                session.Send(new StoC.P281_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = 13,
                    Unknown2 = 13,
                    Unknown3 = new int[] { 0, 8704, 133568, 0, 0, 416074956, 426141286, 429487455, 671088655, 16382 }
                });

                session.Send(new StoC.P282_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = new int[] { 0, 8960, 657856, 0, 0, 416074956, 426141286, 429487455, 316250523, 16383 }
                });

                AgentView.SendFinalize(session, agent.Id);

                CreatureView.SendAgentId(session, creatureId, agent.Id);

                PlayerView.SendBankSlots(session, 1);

                session.Send(new StoC.P055_UnknownMessage()
                {
                    Unknown0 = creatureId,
                    Unknown1 = 2,
                    Unknown2 = 1226
                });

                for (int i = 0; i < 18; ++i)
                    AgentView.SendCoins(session, i, i * 1000);

                AgentView.SendSpawn(session, agent.Id, 273, 16, 1, 7, new Common.Math.WorldPosition(new BulletSharp.Vector3(-18809.6f, 17367.5f, -1226.97f), 0), 8);

                session.Send(new StoC.P650_UnknownMessage()
                {
                    Unknown0 = agent.Id,
                    Unknown1 = 273,
                    Unknown2 = 16
                });

                // NPC TEST

                agent = map.SpawnAgent();
                agent.Level = 1;
                agent.AdjustedLevel = 1;
                agent.Attitude = AttitudeType.kHostile;
                agent.Class = ProfessionType.kNone;
                agent.Health = 100f;
                agent.MaximumHealth = 1000f;
                agent.HealthRegeneration = 10f;
                agent.IsDead = false;
                agent.IsMonster = false;
                agent.IsWeaponDrawn = false;
                agent.IsWearingTownClothes = false;
                agent.Name = "Server Test NPC :-)";
                agent.Party = 50;
                agent.Position = new BulletSharp.Vector4(-18809.6f, 17367.5f, -1226.97f, 0);
                agent.SpeciesDefinition = 536876213;

                AgentModel.SendAgent(session, agent);
            }
        }
    }
}
