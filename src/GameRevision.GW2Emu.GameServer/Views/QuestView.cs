﻿using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class QuestView
    {
        public static void SendPersonalStoryLine(ISession session, int creatureId, short[] storyLine)
        {
            session.Send(new StoC.P235_PersonalStoryQuestsMessage()
            {
                CreatureId = creatureId,
                StoryLine = storyLine
            });
        }

        public static void SendQuestManager(ISession session, int creatureId)
        {
            session.Send(new StoC.P236_ReportQuestManagerMessage()
            {
                CreatureId = creatureId
            });
        }
    }
}
