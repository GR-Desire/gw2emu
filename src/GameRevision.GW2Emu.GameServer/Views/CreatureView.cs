﻿using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class CreatureView
    {
        public static void SendCreateCreature(ISession session, int id, string name, Guid creatureId, int unk3)
        {
            session.Send(new StoC.P179_CreatureCreateMessage()
            {
                CreatureId = id,
                PlayerName = name,
                Unknown2 = creatureId, // CHANGE THAT
                Unknown3 = unk3
            });
        }

        public static void SendAgentId(ISession session, int id, int agentId)
        {
            session.Send(new StoC.P183_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = agentId
            });
        }

        public static void SendPvPGlory(ISession session, int id, int unk)
        {
            session.Send(new StoC.P054_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = unk,
                Unknown2 = unk
            });
        }

        public static void Send184(ISession session, int id, int unk)
        {
            session.Send(new StoC.P184_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = unk
            });
        }

    }
}
