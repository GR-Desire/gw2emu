﻿using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameRevision.GW2Emu.Common.Math;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class MapView
    {
        public static void SendEnterWorld(ISession session, string playerName, int creatureId, short mapId, int serverId, int floor, int gold, BulletSharp.Vector3 spawn)
        {
            MapView.SendMapLoadHeader(session);

            MapView.SendMapLoad(session, playerName, creatureId, mapId, serverId, floor);

            session.Send(new StoC.P611_TradingPostGoldPickupMessage()
            {
                Gold = gold
            });

            MapView.SendMapSpawn(session, spawn, mapId);
        }

        public static void SendCharacterCreationLoad(ISession session)
        {
            MapView.SendMapLoadHeader(session);

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P469_UnknownMessage());

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P727_UnknownMessage()
            {
                Unknown0 = 1
            });

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P589_UnknownMessage()
            {
                Unknown0 = 2488460,
                Unknown1 = 1
            });

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P464_UnknownMessage()
            {
                Unknown0 = 0
            });

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P470_UnknownMessage());
        }

        public static void SendMapLoadHeader(ISession session)
        {
            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P466_MapLoadHeaderMessage() // MapLoadHeader
            {
                Head = 255
            });
        }

        public static void SendMapLoad(ISession session, string playerName, int creatureId, int mapId, int server, int floor)
        {
            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P467_MapLoadPlayerNameMessage() // MapLoadPlayerName
            {
                Name = playerName
            });

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P480_MapLoadServerInfoMessage() // MapLoadServerInfo
            {
                CreatureId = creatureId,
                MapId = mapId,
                Server = server,
                Floor = floor
            });
        }

        public static void SendMapSpawn(ISession session, BulletSharp.Vector3 spawnPoint, short previousMap)
        {
            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P484_MapLoadSpawnPointMessage() // MapLoadSpawnPoint
            {
                SpawnLocation = spawnPoint
            });

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P475_MapLoadPreviousMapMessage() // MapLoadPreviousMap
            {
                MapId = previousMap
            });

            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P479_SpawnInfoCompleteMessage());
        }

        public static void SendMapInformation(ISession session, BulletSharp.Vector3 spawnPoint, short mapId)
        {
            session.Send(new GameRevision.GW2Emu.GameServer.Messages.StoC.P477_MapInfoMessage() // MapInfo
            {
                Unknown0 = spawnPoint, // Map spawn point?
                Unknown1 = new Vector2(0f, 0f),
                Unknown2 = new Vector2(0f, 0f),
                MapId = mapId,
                Unknown4 = 49
            });
        }
    }
}
