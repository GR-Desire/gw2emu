﻿using GameRevision.GW2Emu.Common.Data;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameRevision.GW2Emu.Common.Serialization;
using GameRevision.GW2Emu.Common.Database.DataEntities;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class CharacterCreationView
    {
        public static void Send588(ISession session)
        {
            session.Send(new StoC.P588_UnknownMessage()
            {
                Unknown3 = new StoC.P588_UnknownMessage.Struct0[0]
            });
        }

        public static void Send309(ISession session)
        {
            session.Send(new StoC.P309_UnknownMessage()
            {
                Unknown0 = 3,
                Unknown1 = 2
            });
        }

        public static void SendCharacterInfo(ISession session, Guid characterId, Guid accountId, string name, short unk3, byte[] appearance)
        {
            session.Send(new StoC.P468_UnknownMessage()
            {
                Unknown0 = characterId,
                Unknown1 = accountId,
                Unknown2 = name,
                Unknown3 = 458, // R.E
                Unknown4 = appearance
            });
        }
    }
}
