﻿using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class PlayerView
    {
        public static void SendBankSlots(ISession session, byte slots)
        {
            session.Send(new StoC.P051_BankSlotsMessage()
            {
                Unknown0 = slots
            });
        }
    }
}
