﻿using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class GuildView
    {
        public static void SendDefinition(ISession session, Guid id, string name, string tag)
        {
            session.Send(new StoC.P382_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = name,
                Unknown2 = tag
            });
        }
    }
}
