﻿using GameRevision.GW2Emu.Common.Data;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameRevision.GW2Emu.Common.Serialization;
using GameRevision.GW2Emu.Common.Database.DataEntities;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class ChatView
    {
        public static void SendMessage(ISession session, string message)
        {
            session.Send(new StoC.P307_ChatMessage()
            {
                Message = message
            });
        }

        public static void SendMessageOptions(ISession session, byte type, byte color, string name, Guid playerId)
        {
            session.Send(new StoC.P308_ChatOptionsMessage()
            {
                Type = type,
                Color = color,
                Name = name,
                Id = playerId
            });
        }

        
    }
}
