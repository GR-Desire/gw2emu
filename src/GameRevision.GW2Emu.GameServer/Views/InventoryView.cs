﻿using GameRevision.GW2Emu.Common.Data;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.Common.Session;
using CtoS = GameRevision.GW2Emu.GameServer.Messages.CtoS;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class InventoryView
    {
        public static void SendItem(ISession session, int agentId, character_item item, int registerId)
        {
            switch (item.type)
            {
                case SlotType.kEquipment:
                    SendEquipment(session, agentId, item, registerId);
                    break;
                case SlotType.kBags:
                    SendBags(session, agentId, item, registerId);
                    break;
                case SlotType.kBagSlot:
                    SendBagSlot(session, agentId, item, registerId);
                    break;
            }
        }

        private static void SendEquipment(ISession session, int agentId, character_item item, int registerId)
        {
            switch (item.Definition.type)
            {
                case "Weapon":
                    SendWeapon(session, agentId, item, registerId);
                    break;
                case "Armor":
                    SendArmor(session, agentId, item, registerId);
                    break;
                case "Back":
                    SendBackPiece(session, agentId, item, registerId);
                    break;
                case "Trinket":
                    SendTrinket(session, agentId, item, registerId);
                    break;
            }
        }

        private static void SendWeapon(ISession session, int agentId, character_item item, int itemId)
        {
            session.Send(new StoC.P419_InventoryRegisterWeaponMessage()
            {
                RegisterId = itemId,
                ItemId = item.Definition.id,
                Flags = 56,
                TransmutedDataId = 0,
                UpgradeDataId = 112, // Test 
                Unknown5 = 0,
                Unknown6 = 0,
                Unknown7 = 0,
                Unknown8 = new int[0]
            });

            SendAddItem(session, agentId, itemId, item);
        }

        private static void SendBackPiece(ISession session, int agentId, character_item item, int registerId)
        {
            session.Send(new StoC.P409_InventoryRegisterBackPieceMessage()
            {
                RegisterId = registerId,
                ItemId = 60,
                Flags = 41,
                TransmutedDataId = 0,
                UpgradeDataId = 0, // Test
                Unknown5 = 0,
                Unknown6 = new int[0]
            });

            SendAddItem(session, agentId, registerId, item);
        }

        private static void SendTrinket(ISession session, int agentId, character_item item, int registerId)
        {
            session.Send(new StoC.P417_UnknownMessage()
            {
                Unknown0 = registerId,
                Unknown1 = item.Definition.id,
                Unknown2 = 57,
                Unknown3 = 0,
                Unknown4 = 0, // Test
                Unknown5 = new int[0]
            });

            SendAddItem(session, agentId, registerId, item);
        }

        private static void SendArmor(ISession session, int agentId, character_item item, int registerId)
        {
            session.Send(new StoC.P408_InventoryRegisterArmorMessage()
            {
                RegisterId = registerId,
                ItemId = item.Definition.id,
                Flags = 41,
                TransmutedDataId = 0,
                Colors = new short[] { 13, 211, 0, 0 },
                UpgradeDataId = 24733,
                Unknown6 = 0,
                Unknown7 = new int[0]
            });

            SendAddItem(session, agentId, registerId, item);
        }

        private static void SendBags(ISession session, int agentId, character_item item, int registerId)
        {
            session.Send(new StoC.P416_InventoryRegisterItemMessage()
            {
                RegisterId = registerId,
                ItemId = item.Definition.id,
                Unknown2 = 0,
                Count = (int)item.count
            });

            SendAddItem(session, agentId, registerId, item);
        }

        private static void SendBagSlot(ISession session, int agentId, character_item item, int registerId)
        {
            session.Send(new StoC.P410_InventoryRegisterBagMessage()
            {
                RegisterId = registerId,
                BagId = item.Definition.id,
                Unknown2 = 41
            });

            SendAddItem(session, agentId, registerId, item);
        }

        private static void SendAddItem(ISession session, int agentId, int registerId, character_item item)
        {
            session.Send(new StoC.P124_InventoryAddItemMessage()
            {
                AgentId = agentId,
                ItemId = registerId,
                LocationType = item.type,
                Position = item.position
            });
        }
    }
}
