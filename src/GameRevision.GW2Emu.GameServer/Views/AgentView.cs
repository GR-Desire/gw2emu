﻿using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoC = GameRevision.GW2Emu.GameServer.Messages.StoC;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class AgentView
    {
        public static void SendLocalAgentId(ISession session, short id)
        {
            session.Send(new StoC.P047_LocalPlayerAgentIdMessage()
            {
                AgentId = id
            });
        }

#region Agent start and end

        public static void SendCreate(ISession session, short id, int unk3, bool isMonster, int speciesId, byte party, byte type2, WorldPosition spawnPosition, short unk11, float[] unk12)
        {
            session.Send(new StoC.P012_AgentCreateMessage()
            {
                AgentId = id,
                SequenceId = 16983,
                Unknown2 = 128,
                Unknown3 = unk3,
                Unknown4 = new byte[0],
                IsMonster = isMonster ? (byte)1 : (byte)0,
                SpeciesId = speciesId,
                Party = party,
                Unknown8 = type2,
                SpawnPosition = spawnPosition,
                Unknown10 = 0,
                Unknown11 = unk11,
                Unknown12 = unk12,
                Unknown13 = 24f
            });
        }

        public static void SendSpawn(ISession session, short id, int sequenceId, byte type, byte party, byte creatureId, WorldPosition spanwPosition, short unk7)
        {
            session.Send(new StoC.P019_AgentSpawnMessage()
            {
                AgentId = id,
                SequenceId = sequenceId,
                Unknown2 = type,
                Party = party,
                CreatureId = creatureId,
                SpawnLocation = spanwPosition,
                Unknown6 = 0,
                Unknown7 = unk7
            });
        }

        public static void SendFinalize(ISession session, int id)
        {
            session.Send(new StoC.P067_AgentFinalizeMessage()
            {
                AgentId = id,
            });
        }

#endregion

        public static void SendAssignCreature(ISession session, int id, byte creatureId)
        {
            session.Send(new StoC.P182_CreatureAssignToAgentMessage()
            {
                CreatureId = creatureId,
                AgentId = id
            });
        }

        public static void SendAttunement(ISession session, int id, int attunement)
        {
            session.Send(new StoC.P216_AgentAttunementMessage()
            {
                AgentId = id,
                Unknown1 = attunement
            });
        }

        public static void SendAttitude(ISession session, int id, int attitude)
        {
            session.Send(new StoC.P061_AgentAttitudeMessage()
            {
                AgentId = id,
                Attitude = attitude,
            });
        }

        public static void SendDownedPenalty(ISession session, int id, byte penalty)
        {
            session.Send(new StoC.P091_AgentDownedPenaltyMessage()
            {
                AgentId = id,
                Penalty = penalty
            });
        }

        public static void SendHealth(ISession session, int id, bool isDead, float health, float maxHealth, float regeneration)
        {
            session.Send(new StoC.P104_AgentHealthMessage()
            {
                AgentId = id,
                IsDead = isDead ? (byte)1 : (byte)0,
                Health = health,
                MaximumHealth = maxHealth,
                Regeneration = regeneration
            });
        }

        public static void SendEndurance(ISession session, int id, int endurance, int maxEndurance, int unkEndurance)
        {
            session.Send(new StoC.P096_AgentEnduranceMessage()
            {
                AgentId = id,
                Unknown1 = endurance,
                Unknown2 = maxEndurance,
                Unknown3 = unkEndurance
            });
        }

        public static void SendInventory119(ISession session, int id)
        {
            session.Send(new StoC.P119_UnknownMessage()
            {
                Unknown0 = id,
            });
        }

        public static void SendInventory136(ISession session, int id, byte isUnk1)
        {
            session.Send(new StoC.P136_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = isUnk1
            });
        }

        public static void SendDeathInfo(ISession session, int id, bool isDead)
        {
            session.Send(new StoC.P070_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = isDead ? (byte)1 : (byte)0
            });
        }

        public static void SendWeaponsOut(ISession session, int id, bool isWeaponOut)
        {
            session.Send(new StoC.P071_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = isWeaponOut ? (byte)1 : (byte)0
            });
        }

        public static void SendRegisterInventory(ISession session, int id)
        {
            session.Send(new StoC.P118_AgentRegisterInventoryMessage()
            {
                AgentId = id
            });
        }

        public static void SendArmorVisibility(ISession session, int id, int[] masks)
        {
            session.Send(new StoC.P133_AgentArmorVisibilityMessage()
            {
                AgentId = id,
                VisibilityMask = masks
            });
        }

        public static void SendDefaultWeaponSet(ISession session, int id, byte isUnderWater, byte weaponSet)
        {
            session.Send(new StoC.P134_AgentDefaultWeaponsetMessage()
            {
                AgentId = id,
                Unknown1IsUnderWater = isUnderWater,
                WeaponSet = weaponSet
            });
        }

        public static void SendCurrentEquippedWeaponSet(ISession session, int id, byte weaponSet)
        {
            session.Send(new StoC.P129_AgentCurrentEquippedWeaponsetMessage()
            {
                AgentId = id,
                WeaponSet = weaponSet
            });
        }

        public static void SendGatherSlot(ISession session, int id, byte slots)
        {
            session.Send(new StoC.P128_AgentGatherSlotMessage()
            {
                AgentId = id,
                Slots = slots
            });
        }

        public static void SendUnlockBags(ISession session, int id, byte bags)
        {
            session.Send(new StoC.P131_AgentUnlockedBagsMessage()
            {
                AgentId = id,
                BagsAllowed = bags
            });
        }

        public static void Send126(ISession session, int id, byte unk)
        {
            session.Send(new StoC.P126_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = unk
            });
        }

        public static void SendInventorySlots(ISession session, int id, int slots)
        {
            session.Send(new StoC.P135_AgentInventorySlotsMessage()
            {
                AgentId = id,
                Unknown1 = slots
            });
        }

        public static void SendWearingTownClothes(ISession session, int id, bool isWearing)
        {
            session.Send(new StoC.P127_AgentWearingTownClothesMessage()
            {
                AgentId = id,
                IsWearingTownClothes = isWearing ? (byte)1 : (byte)0
            });
        }

        public static void SendPlayerCoreStats(ISession session, int id, byte[] appearance, byte level, byte adjustedLevel, byte @class, int totalExperience, StoC.P072_AgentPlayerCoreStatsMessage.Struct6[] stats, float charm, float ferocity, float dignity)
        {
            session.Send(new StoC.P072_AgentPlayerCoreStatsMessage()
            {
                AgentId = id,
                Appearance = appearance,
                Level = level,
                AdjustedLevel = adjustedLevel,
                @Class = @class,
                TotalExperience = totalExperience,
                Unknown10 = stats,
                Charm = charm,
                Dignity = dignity,
                Ferocity = ferocity
            });
        }

        public static void SendCoreStats(ISession session, int id, byte[] appearance, byte level, byte adjustedLevel, byte @class)
        {
            session.Send(new StoC.P073_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = appearance,
                Unknown2 = level,
                Unknown3 = adjustedLevel,
                Unknown4 = @class
            });
        }

        public static void SendCoins(ISession session, int type, int amount)
        {
            session.Send(new StoC.P052_BankGoldMessage()
            {
                Type = type,
                Amount = amount
            });
        }

        public static void SendPlayerInfo289(ISession session, int id, int unk1, int playerId)
        {
            session.Send(new StoC.P289_UnknownMessage()
            {
                Unknown0 = id,
                Unknown1 = unk1,
                Unknown2 = playerId
            });
        }

        public static void SendPetInformation(ISession session, int id, byte unk1, byte unk2, int[] stats)
        {
            session.Send(new StoC.P142_AgentPetInfoMessage()
            {
                AgentId = id,
                Unknown1 = unk1,
                Unknown2 = unk2,
                Unknown3 = stats
            });
        }

        public static void SendRegisterPet(ISession session, int id, byte unk1, string petName, int unk3, int unk4)
        {
            session.Send(new StoC.P147_AgentRegisterPetMessage()
            {
                AgentId = id,
                Unknown1 = unk1,
                PetName = petName,
                Unknown3 = unk3,
                Unknown4 = unk4
            });
        }

        public static void SendRegisterSkillBar(ISession session, int id)
        {
            session.Send(new StoC.P268_AgentRegisterSkillBarMessage()
            {
                AgentId = id
            });
        }
    }
}
