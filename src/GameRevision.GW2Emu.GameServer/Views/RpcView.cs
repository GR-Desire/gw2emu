﻿using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Messaging.Rpc;

namespace GameRevision.GW2Emu.GameServer.Views
{
    public static class RpcView
    {
        public static void SendRegisterServer(ISession session, int id, int type)
        {
            session.Send(new P1002_RegisterServerMessage()
                {
                    Id = id,
                    Type = type
                });
        }
    }
}
