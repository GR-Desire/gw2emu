using System;
using System.Timers;
using System.Threading;

namespace GameRevision.GW2Emu.GameServer
{
    public static class Program
    {
        public static void Main()
        {
            try
            {
                Console.Title = "GW2Emu game server";

                log4net.Config.XmlConfigurator.Configure();

                // register the event handlers
                GameServerApp.Instance.RegisterHandlers();

                // start the server
                GameServerApp.Instance.Start();

                while (!Console.KeyAvailable)
                {
                    GameServerApp.Instance.UpdateOneFrame(0.0f);
                    Thread.Sleep(50);
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.ReadKey(true);

        }
    }
}
