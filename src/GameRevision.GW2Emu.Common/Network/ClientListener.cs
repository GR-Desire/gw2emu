using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;

namespace GameRevision.GW2Emu.Common.Network
{
    public sealed class ClientListener
    {
        private static ILog logger = LogManager.GetLogger(typeof(ClientListener));

        public event EventHandler<ClientConnectedEventArgs> ClientConnected;
        public IPEndPoint EndPoint { get; private set; }

        public bool Listening
        {
            get
            {
                return this.listening;
            }
        }

        private const int Backlog = 100;

        private event Action Stopped;
        private Socket socket;
        private volatile bool listening;

        public ClientListener(IPAddress address, int port)
        {
            this.EndPoint = new IPEndPoint(address, port);

            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                this.socket.Bind(this.EndPoint);
            }
            catch (SocketException ex)
            {
                logger.FatalFormat("Couldn't bind listener socket to the specified IP address and port.\nError code: {0}\n{1}\n{2}", ex.ErrorCode, ex.Message, ex.StackTrace);
            }
            catch (Exception ex)
            {
                logger.FatalFormat("An unknown exception has occured:\n{0}\n{1}", ex.Message, ex.StackTrace);
            }
        }

        private void OnClientConnected(Client client)
        {
            if (this.ClientConnected != null)
            {
                this.ClientConnected(this, new ClientConnectedEventArgs(client));
            }
        }

        private void OnStopped()
        {
            if (this.Stopped != null)
            {
                this.Stopped();
            }
        }

        public void Listen()
        {
            this.listening = true;

            try
            {
                this.socket.Listen(Backlog);
            }
            catch (SocketException ex)
            {
                logger.FatalFormat("Failed to start listening.\nError code: {0}\n{1}\n{2}", ex.ErrorCode, ex.Message, ex.StackTrace);

                this.Stop();
            }

            if (this.listening)
            {
                this.socket.BeginAccept(this.AcceptCallback, null);
            }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                Client client = new Client(this.socket.EndAccept(ar));

                this.Stopped += delegate
                {
                    client.Disconnect();
                };

                logger.Info("Client connected");

                this.OnClientConnected(client);

                if (this.listening)
                {
                    this.socket.BeginAccept(this.AcceptCallback, null);
                }
            }
            catch (SocketException ex)
            {
                logger.ErrorFormat("The socket threw an exception while accepting a client:\nError code: {0}\n{1}\n{2}", ex.ErrorCode, ex.Message, ex.StackTrace);
            }
            catch (Exception ex)
            {

            }
        }

        public void Stop()
        {
            this.listening = false;

            this.socket.Close();

            this.OnStopped();
        }
    }
}

