﻿using System;

namespace GameRevision.GW2Emu.Common.Network
{
    public class AsyncState
    {
        public byte[] Buffer { get; private set; }

        public AsyncState()
        {
            this.Buffer = new byte[8192];
        }

        public AsyncState(byte[] buffer)
        {
            this.Buffer = buffer;
        }

        public byte[] GetResizedBuffer(int size)
        {
            byte[] buffer = new byte[size];

            Array.Copy(this.Buffer, buffer, size);

            return buffer;
        }
    }
}
