﻿using log4net;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace GameRevision.GW2Emu.Common.Network
{
    public sealed class Client
    {
        private static ILog logger = LogManager.GetLogger(typeof(Client));

        public event EventHandler<DataReceivedEventArgs> DataReceived;
        public event EventHandler<ClientDisconnectedEventArgs> Disconnected;

        public IPEndPoint RemoteEndPoint { get; private set; }
        public IPEndPoint LocalEndPoint { get; private set; }

        public bool Connected
        {
            get
            {
                return this.connected;
            }
        }

        public Socket Socket
        {
            get
            {
                return socket;
            }
        }

        private Socket socket;
        private volatile bool connected;

        public Client(Socket socket)
        {
            this.socket = socket;
            this.connected = true;

            this.RemoteEndPoint = (IPEndPoint)this.socket.RemoteEndPoint;
            this.LocalEndPoint = (IPEndPoint)this.socket.LocalEndPoint;
        }

        private void OnDataReceived(byte[] data)
        {
            if (this.DataReceived != null)
            {
                this.DataReceived(this, new DataReceivedEventArgs(data));
            }
        }

        private void OnDisconnected()
        {
            if (this.Disconnected != null)
            {
                this.Disconnected(this, new ClientDisconnectedEventArgs());
            }
        }

        public void BeginReceive()
        {
            AsyncState state = new AsyncState();

            if (this.connected)
            {
                this.socket.BeginReceive(state.Buffer, 0, state.Buffer.Length, SocketFlags.None, this.ReceiveCallback, state);
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                int bytesRead = this.socket.EndReceive(ar);

                if (bytesRead == 0)
                {
                    this.Disconnect();
                    return;
                }

                AsyncState state = (AsyncState)ar.AsyncState;

                byte[] buffer = state.GetResizedBuffer(bytesRead);

                this.OnDataReceived(buffer);

                if (this.connected)
                {
                    this.socket.BeginReceive(state.Buffer, 0, state.Buffer.Length, SocketFlags.None, this.ReceiveCallback, state);
                }
            }
            catch (SocketException ex)
            {
                if (ex.SocketErrorCode == SocketError.ConnectionReset)
                {
                    this.Disconnect();
                }
                else
                {
                    logger.ErrorFormat("The socket threw an exception while receiving data:\nError code: {0}\n{1}\n{2}", ex.ErrorCode, ex.Message, ex.StackTrace);
                    
                    this.Disconnect();
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("An unknown exception has occured:\n{0}\n{1}", ex.Message, ex.StackTrace);
                
                this.Disconnect();
            }
        }

        public bool Send(byte[] buffer)
        {
            if (this.connected)
            {
                try
                {
                    int bytesSent = this.socket.Send(buffer);

                    if (bytesSent == 0)
                    {
                        this.Disconnect();

                        return false;
                    }

                    return true;
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode == SocketError.ConnectionReset)
                    {
                        this.Disconnect();
                    }
                    else
                    {
                        logger.ErrorFormat("The socket threw an exception while sending data:\nError code: {0}\n{1}\n{2}", ex.ErrorCode, ex.Message, ex.StackTrace);

                        this.Disconnect();
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("An unknown exception has occured:\n{0}\n{1}", ex.Message, ex.StackTrace);

                    this.Disconnect();
                }
            }

            return false;
        }

        public void Disconnect()
        {
            this.connected = false;

            this.socket.Close();

            this.OnDisconnected();

            logger.Info("Client disconnected");
        }
    }
}
