﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameRevision.GW2Emu.Common.Data
{
    public static class CoinType
    {
        public static int kGold = 1;
        public static int kKarma = 2;
        public static int kLaurels = 3;
        public static int kGems = 4;
        public static int kAscalonianTears = 5;
        public static int kZhaitanShard = 6;
        public static int kFractalRelics = 7;
        public static int kInfluence = 8; // unsure about that
        public static int kBeetletunSeals = 9;
        public static int kMoletariatekManifesto = 10;
        public static int kDeadlyBloom = 11;
        public static int kKodaSymbols = 12;
        public static int kCharrCarving = 13;
        public static int kKnowledgeCrystals = 14;
        public static int kHonnorBadges = 15;
        public static int kGuildMerit = 16;
        public static int kGlory = 17; // HOLE - LOL
    }
}
