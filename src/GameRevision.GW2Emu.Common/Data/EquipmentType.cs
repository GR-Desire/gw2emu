﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameRevision.GW2Emu.Common.Data
{
    public static class EquipmentType
    {
        public static byte kUnderWaterHead = 0;
        public static byte kBackPiece = 1;
        public static byte kBodyChest = 2;
        public static byte kBodyFeet = 3;
        public static byte kBodyHands = 4;
        public static byte kBodyHead = 5;
        public static byte kBodyLegs = 6;
        public static byte kBodyShoulders = 7;
        public static byte kUnk8 = 8;
        public static byte kAccessory1 = 19;
        public static byte kAccessory2 = 20;
        public static byte kRing1 = 21;
        public static byte kRing2 = 22;
        public static byte kAmulet = 23;
        public static byte kUnderWaterWeapon1 = 24;
        public static byte kUnderWaterWeapon2 = 25;
        public static byte kWeapon1RightHand = 29;
        public static byte kWeapon1LeftHand = 30;
        public static byte kWeapon2RightHand = 31;
        public static byte kWeapon2LeftHand = 32;
    }
}
