﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Data
{
    public static class AttitudeType
    {
        public static int kFriendly = 0;
        public static int kHostile = 1;
        public static int kIndifferent = 2;
        public static int kNeutral = 3;
    }
}
