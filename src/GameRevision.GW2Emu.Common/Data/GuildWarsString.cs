﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Data
{
    public static class GuildWarsString
    {
        public static string ToWordBit(this string str)
        {
            return Encoding.Unicode.GetString(new byte[] { 0x01, 0x81, 0x7b, 0x44, 0x7, 0x1 }) + str + Encoding.Unicode.GetString(new byte[] { 0x1, 0x0 });
        }
    }
}
