﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameRevision.GW2Emu.Common.Data
{
    public static class SlotType
    {
        public const byte kEquipment = 2;
        public const byte kBags = 3;
        public const byte kGatheringTools = 4;
        public const byte kBagSlot = 5;
    }

}
