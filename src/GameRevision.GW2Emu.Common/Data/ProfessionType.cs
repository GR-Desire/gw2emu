﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Data
{
    public static class ProfessionType
    {
        public static byte kWarrior = 1;
        public static byte kGuardian = 2;
        public static byte kEngineer = 3;
        public static byte kRanger = 4;
        public static byte kThief = 5;
        public static byte kElementalist = 6;
        public static byte kMesmer = 7;
        public static byte kNecromancer = 8;
        public static byte kNone = 9;
    }
}
