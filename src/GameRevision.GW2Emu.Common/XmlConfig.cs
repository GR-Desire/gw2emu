﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace GameRevision.GW2Emu.Common
{
    public static class XmlConfig
    {
        public static bool Exists(string fileName)
        {
            return File.Exists(fileName);
        }            

        public static T Load<T>(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stream);
            }
        }

        public static void Save<T>(string fileName, T value)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stream, value);
            }
        }
    }
}
