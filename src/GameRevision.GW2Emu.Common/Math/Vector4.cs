﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Math
{
    public static class Vector4Ext
    {
        public static BulletSharp.Vector4 ConvertToBigCoordinates(this BulletSharp.Vector4 v)
        {
            return new BulletSharp.Vector4(v.X * 32f, v.Y * 32f, -v.Z * 32f, v.W);
        }

        public static BulletSharp.Vector4 ConvertToSmallCoordinates(this BulletSharp.Vector4 v)
        {
            return new BulletSharp.Vector4(v.X / 32f, v.Y / 32f, -v.Z / 32f, v.W);
        }

        public static BulletSharp.Vector3 ConvertToVector3(this BulletSharp.Vector4 v)
        {
            return new BulletSharp.Vector3(v.X, v.Y, v.Z);
        }

        public static WorldPosition ConvertToWorldPosition(this BulletSharp.Vector4 v)
        {
            return new WorldPosition(v.ConvertToVector3(), (int)v.W);
        }
    }
}
