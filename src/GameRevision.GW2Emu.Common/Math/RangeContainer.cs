﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;

namespace GameRevision.GW2Emu.Common.Math
{
    public class RangeContainer
    {
        private List<int> usedValues;
        private readonly object listLock;
        private int maxValue;
        private int minValue;

        public RangeContainer(int min, int max)
        {
            this.listLock = new object();
            this.maxValue = max;
            this.minValue = min;
            this.usedValues = new List<int>();
        }

        public RangeContainer(int min)
            : this(min, int.MaxValue)
        {
        }

        public RangeContainer() : this(1, int.MaxValue)
        {
        }

        public int Acquire()
        {
            try
            {
                lock(this.listLock)
                {
                    var v = Enumerable.Range(this.minValue, this.maxValue - this.minValue)
                                .Except(this.usedValues)
                                .FirstOrDefault();
                    this.usedValues.Add(v);
                    return v;
                }
            }
            catch
            {
            }
            return 0;
        }

        public void Release(int v)
        {
            try
            {
                lock (this.listLock)
                {
                    this.usedValues.Remove(v);
                }
            }
            catch
            {
            }
        }
    }
}
