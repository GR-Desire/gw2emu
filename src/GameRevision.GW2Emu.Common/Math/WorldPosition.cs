﻿using System;

namespace GameRevision.GW2Emu.Common.Math
{
    public struct WorldPosition
    {

        public BulletSharp.Vector3 Vector;
        public int W;

        public WorldPosition(BulletSharp.Vector3 vector, int w)
        {
            Vector = vector;
            W = w;
        }
    }
}
