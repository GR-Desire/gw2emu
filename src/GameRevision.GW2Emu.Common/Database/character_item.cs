//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GameRevision.GW2Emu.Common.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class character_item
    {
        public character_item()
        {
            this.item = 0;
            this.flags = 0;
        }
    
        public int id { get; set; }
        public System.Guid character { get; set; }
        public int item { get; set; }
        public int flags { get; set; }
        public int color1 { get; set; }
        public int color2 { get; set; }
        public int color3 { get; set; }
        public int color4 { get; set; }
        public byte type { get; set; }
        public byte position { get; set; }
        public long count { get; set; }
        public byte[] specific { get; set; }
    
        public virtual item Definition { get; set; }
        public virtual character Owner { get; set; }
    }
}
