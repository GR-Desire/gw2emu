﻿using System;
using GameRevision.GW2Emu.Common.Serialization;
using GameRevision.GW2Emu.Common.Data;

namespace GameRevision.GW2Emu.Common.Database.DataEntities
{
    public static class CharacterData
    {
        // First byte is always 07

        public enum Class
        {
            kGardian = 1,
            kThief = 5,
            kElementalist = 6,
            kNecro = 8
        }

        public static void Deserialize(Deserializer deserializer, ref character character)
        {
            character.unknown0 = deserializer.ReadByte();
            character.mapId = deserializer.ReadUInt16();
            character.mapCompletion = deserializer.ReadByte();
            character.level = deserializer.ReadByte();
            character.@class = deserializer.ReadByte();
            character.appearance = deserializer.ReadBytes(45);
        }

        public static void Serialize(Serializer serializer, character character)
        {
            serializer.Write(character.unknown0);
            serializer.Write((ushort)character.mapId);
            serializer.Write(character.mapCompletion);
            serializer.Write(character.level);
            serializer.Write(character.@class);
            serializer.Write(character.appearance);
            Item.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kWeapon1RightHand));
            Item.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kWeapon1LeftHand));
            serializer.Write((byte)0); // unk
            serializer.Write((byte)0); // unk
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kBodyChest));
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kBodyFeet));
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kUnk8));
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kBodyHands));
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kBodyLegs));
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kBodyShoulders));
            ColoredItem.Serialize(serializer, CharacterAccessor.GetEquippedItemBySlot(character, EquipmentType.kBodyHead));
            serializer.Write(character.unknown1);
            serializer.Write(character.unknown2);
        }
    }
}
