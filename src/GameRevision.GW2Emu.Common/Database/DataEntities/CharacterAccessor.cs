﻿using GameRevision.GW2Emu.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Database.DataEntities
{
    public static class CharacterAccessor
    {
        public static character_item GetEquippedItemBySlot(character character, int slot)
        {
            var item = (from i in character.Items
                       where i.position == slot && i.type == SlotType.kEquipment
                       select i).SingleOrDefault();

            if (item == null)
                return new character_item();

            return item;
        }
    }
}
