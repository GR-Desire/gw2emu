﻿using System;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.Common.Database.DataEntities
{
    public static class ColoredItem
    {
        public static void Deserialize(Deserializer deserializer, ref character_item item)
        {
            Item.Deserialize(deserializer, ref item);
            item.color1 = deserializer.ReadUInt16();
            item.color2 = deserializer.ReadUInt16();
            item.color3 = deserializer.ReadUInt16();
            item.color4 = deserializer.ReadUInt16();
        }

        public static void Serialize(Serializer serializer, character_item item)
        {
            Item.Serialize(serializer, item);
            serializer.Write((ushort)item.color1);
            serializer.Write((ushort)item.color2);
            serializer.Write((ushort)item.color3);
            serializer.Write((ushort)item.color4);
        }
    }
}
