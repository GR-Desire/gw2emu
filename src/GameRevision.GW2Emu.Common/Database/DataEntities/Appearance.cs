﻿using System;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.Common.Database.DataEntities
{
    public class Appearance
    {
        public byte[] Appearance1 = new byte[29];
        public byte Gender;
        public byte[] Appearance2 = new byte[15];

        public void Deserialize(Deserializer deserializer)
        {
            this.Appearance1 = deserializer.ReadBytes(this.Appearance1.Length);
            this.Gender = deserializer.ReadByte();
            this.Appearance2 = deserializer.ReadBytes(this.Appearance2.Length);
        }

        public void Serialize(Serializer serializer)
        {
            serializer.Write(this.Appearance1);
            serializer.Write(this.Gender);
            serializer.Write(this.Appearance2);
        }
    }
}
