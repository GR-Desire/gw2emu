﻿using System;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.Common.Database.DataEntities
{
    public static class Item
    {
        public enum Flag : ushort
        {
            kVisible = 0,
            kHidden = 0x8000
        }

        public static void Deserialize(Deserializer deserializer, ref character_item item)
        {
            item.item = deserializer.ReadUInt16();
            item.flags = deserializer.ReadUInt16();
        }

        public static void Serialize(Serializer serializer, character_item item)
        {
            serializer.Write((ushort)item.item);
            serializer.Write((ushort)item.flags);
        }
    }
}
