﻿using System;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.Common.Messaging
{
    public abstract class GenericMessage : IMessage
    {
        public ISession Session { get; set; }
        public abstract ushort Header { get; }

        public virtual void Deserialize(Deserializer deserializer)
        {
            throw new NotImplementedException();
        }

        public virtual void Serialize(Serializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
