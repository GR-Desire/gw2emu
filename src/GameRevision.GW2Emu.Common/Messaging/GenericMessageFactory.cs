using System;
using GameRevision.GW2Emu.Common.Serialization;
using System.Collections.Generic;

namespace GameRevision.GW2Emu.Common.Messaging
{
    public abstract class GenericMessageFactory : IMessageFactory
    {
        public IMessage CreateMessage(byte[] data, out int consumed)
        {
            var deserializer = new Deserializer((byte[])data.Clone());

            consumed = 0;

            if (!deserializer.EndOfStream)
            {
                try
                {
                    IMessage message = this.CreateEmptyMessage(deserializer.ReadUInt16());

                    message.Deserialize(deserializer);

                    consumed = (int)deserializer.BaseStream.Position;

                    return message;
                }
                catch
                {
                }                
            }

            return null;
        }

        protected abstract IMessage CreateEmptyMessage(ushort header);
    }
}

