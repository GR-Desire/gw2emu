﻿using GameRevision.GW2Emu.Common.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Messaging.Rpc
{
    public class P0001_CreateSessionMessage : GenericMessage
    {
        public string Email;
        public int Mode;

        public override ushort Header
        {
            get
            {
                return 1;
            }
        }

        public override void Serialize(Serializer serializer)
        {
            serializer.Write(Header);
            serializer.Write(Email);
            serializer.Write(Mode);
        }

        public override void Deserialize(Deserializer deserializer)
        {
            this.Email = deserializer.ReadString();
            this.Mode = deserializer.ReadInt32();
        }
    }
}
