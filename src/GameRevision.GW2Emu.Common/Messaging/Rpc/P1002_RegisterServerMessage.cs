﻿using GameRevision.GW2Emu.Common.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Messaging.Rpc
{
    public class P1002_RegisterServerMessage : GenericMessage
    {
        public int Id;
        public int Type;

        public override ushort Header
        {
            get
            {
                return 1002;
            }
        }

        public override void Serialize(Serializer serializer)
        {
            serializer.Write(Header);
            serializer.Write(Id);
            serializer.Write(Type);
        }

        public override void Deserialize(Deserializer deserializer)
        {
            this.Id = deserializer.ReadInt32();
            this.Type = deserializer.ReadInt32();
        }
    }
}
