﻿using GameRevision.GW2Emu.Common.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Messaging.Rpc
{
    public class P1001_CreateSessionStatusMessage : GenericMessage
    {
        public int Code;

        public override ushort Header
        {
            get
            {
                return 1001;
            }
        }

        public override void Serialize(Serializer serializer)
        {
            serializer.Write(Header);
            serializer.Write(Code);
        }

        public override void Deserialize(Deserializer deserializer)
        {
            this.Code = deserializer.ReadInt32();
        }
    }
}
