using System;
using System.Collections.Generic;

namespace GameRevision.GW2Emu.Common.Messaging
{
    public interface IMessageFactory
    {
        IMessage CreateMessage(byte[] data, out int consumed);
    }
}

