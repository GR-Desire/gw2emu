﻿using System;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Serialization;
using GameRevision.GW2Emu.Common.Session;

namespace GameRevision.GW2Emu.Common.Messaging
{
    public interface IMessage : IEvent
    {
        ISession Session { get; set; }
        ushort Header { get; }

        void Serialize(Serializer serializer);
        void Deserialize(Deserializer deserializer);
    }
}
