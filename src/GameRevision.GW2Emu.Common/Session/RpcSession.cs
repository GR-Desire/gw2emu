﻿using System;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Network;
using GameRevision.GW2Emu.Common.Serialization;
using GameRevision.GW2Emu.Common.Messaging;
using System.Collections.Generic;
using GameRevision.GW2Emu.LoginServer.Messages;
using System.Net;
using System.Net.Sockets;
using log4net;

namespace GameRevision.GW2Emu.GameServer.Session
{
    public class RpcSession : ISession
    {
        private static ILog logger = LogManager.GetLogger(typeof(RpcSession));

        public object Attachment { get; set; }

        private Client client;
        private IEventAggregator aggregator;
        private IMessageFactory messageFactory;
        private byte[] buffer;

        public RpcSession(Client client, IEventAggregator aggregator)
        {
            this.client = client;
            this.client.Socket.NoDelay = true;
            this.client.Disconnected += this.OnDisconnected;
            this.client.DataReceived += this.OnDataReceived;

            this.aggregator = aggregator;
            this.messageFactory = new RpcMessageFactory();
            this.buffer = new byte[0];
        }

        public RpcSession(string ip, int port, IEventAggregator aggregator)
        {
            try
            {
                IPAddress address;

                if (!IPAddress.TryParse(ip, out address))
                {
                    address = IPAddress.Any;

                    logger.Error("Couldn't parse IP address from config. Running on default.");
                }

                Socket rpcSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                rpcSocket.Connect(address, port);
                if (!rpcSocket.Connected)
                {
                    throw new Exception("Unable to connect to the Login Server.");
                }

                this.client = new Client(rpcSocket);
                this.client.Disconnected += this.OnDisconnected;
                this.client.DataReceived += this.OnDataReceived;

                this.aggregator = aggregator;
                this.messageFactory = new RpcMessageFactory();
            }
            catch (System.Exception ex)
            {
                throw new Exception("Unable to connect to the Login Server.", ex);
            }

        }

        public bool Send(IMessage message)
        {
            Serializer serializer = new Serializer();

            message.Serialize(serializer);

            if (this.Send(serializer.GetBytes()))
            {
                return true;
            }

            return false;
        }

        public bool Send(byte[] buffer)
        {
            return this.client.Send(buffer);
        }

        public void Start()
        {
            this.client.BeginReceive();
        }

        public void Disconnect()
        {
            this.client.Disconnect();
        }

        private void OnDataReceived(object sender, DataReceivedEventArgs e)
        {
            byte[] tmp = e.Buffer;

            this.buffer = this.buffer.Append(tmp);

            int consumed;

            do
            {
                IMessage message = this.messageFactory.CreateMessage(this.buffer, out consumed);

                if (consumed == 0)
                    break;

                this.buffer = this.buffer.SubArray(consumed);

                message.Session = this;
                this.aggregator.Trigger(message);

            } while (true);    
        }

        private void OnDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            this.aggregator.Trigger(new ClientDisconnectedEvent(this));
        }

        public void Update(float delta)
        {

        }
    }
}
