﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Session
{
    public static class ArrayExt
    {
        public static T[] SubArray<T>(this T[] data, int index)
        {
            if(index >= data.Length)
                return new T[0];

            T[] result = new T[data.Length - index];
            System.Buffer.BlockCopy(data, index, result, 0, result.Length);
            return result;
        }

        public static T[] Append<T>(this T[] array, T[] append)
        {
            T[] rv = new T[array.Length + append.Length];
            System.Buffer.BlockCopy(array, 0, rv, 0, array.Length);
            System.Buffer.BlockCopy(append, 0, rv, array.Length, append.Length);
            return rv;
        }
    }
}
