﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.Common.Session
{
    public class SessionManager
    {
        private List<ISession> sessions;
        private ConcurrentQueue<ISession> sessionToAdd;
        private ConcurrentQueue<ISession> sessionToRemove;

        public int Count
        {
            get
            {
                return this.sessions.Count;
            }
        }

        public SessionManager()
        {
            this.sessions = new List<ISession>();
            this.sessionToAdd = new ConcurrentQueue<ISession>();
            this.sessionToRemove = new ConcurrentQueue<ISession>();
        }

        public void Add(ISession session)
        {
            this.sessionToAdd.Enqueue(session);
        }

        public void Remove(ISession session)
        {
            this.sessionToRemove.Enqueue(session);
        }

        public void AddFlush(ISession session)
        {
            this.sessions.Add(session);
        }

        public void RemoveFlush(ISession session)
        {
            this.sessions.Remove(session);
        }

        public void Flush()
        {
            ISession session;
            while (this.sessionToRemove.TryDequeue(out session))
            {
                this.sessions.Remove(session);
            }

            while (this.sessionToAdd.TryDequeue(out session))
            {
                this.sessions.Add(session);
            }
        }

        public void Iterate(Action<ISession> func)
        {
            foreach (var s in this.sessions)
            {
                func(s);
            }
        }

        public void Update(float delta)
        {
            foreach (var s in this.sessions)
            {
                s.Update(delta);
            }
        }
    }
}
