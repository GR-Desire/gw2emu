using System;
using System.Collections.Generic;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Network;
using GameRevision.GW2Emu.Common.Messaging;
using GameRevision.GW2Emu.Common.Cryptography;
using GameRevision.GW2Emu.Common.Serialization;
using System.Collections.Concurrent;
using log4net;

namespace GameRevision.GW2Emu.Common.Session
{
    public abstract class GenericSession : ISession
    {
        private static ILog logger = LogManager.GetLogger(typeof(GenericSession));

        public object Attachment { get; set; }

        private Client client;
        private IEventAggregator aggregator;
        private IMessageFactory messageFactory;
        private Handshake handshake;
        private RC4Encryptor encryptor;
        private LZ4Compressor compressor;
        private ConcurrentQueue<IMessage> messages;
        private byte[] buffer;

        public GenericSession(Client client, IEventAggregator aggregator, IMessageFactory messageFactory, Handshake handshake)
        {
            this.client = client;
            this.client.Disconnected += this.OnDisconnected;

            this.aggregator = aggregator;
            this.messageFactory = messageFactory;
            this.handshake = handshake;
            this.messages = new ConcurrentQueue<IMessage>();
            this.buffer = new byte[0];

            logger.Debug("Session created.");

            this.handshake.HandshakeDone += delegate
            {
                this.encryptor = new RC4Encryptor(handshake.EncryptionKey);
                this.compressor = new LZ4Compressor();
                this.client.DataReceived += this.OnDataReceived;

                logger.Debug("Handshake done.");
            };
        }

        public bool Send(IMessage message)
        {
            Serializer serializer = new Serializer();

            message.Serialize(serializer);

            if (this.Send(serializer.GetBytes()))
            {
                logger.DebugFormat("-> StoC packet, header: {0}", message.Header);

                return true;
            }

            return false;
        }

        public bool Send(byte[] buffer)
        {
            byte[] compressed = this.compressor.Compress(buffer);
            byte[] encrypted = this.encryptor.Encrypt(compressed);

            return this.client.Send(encrypted);
        }

        public void Start()
        {
            this.client.BeginReceive();
        }

        public void Disconnect()
        {
            this.client.Disconnect();
        }

        private void OnDataReceived(object sender, DataReceivedEventArgs e)
        {
            byte[] tmp = this.encryptor.Decrypt(e.Buffer);
            this.buffer = this.buffer.Append(tmp);

            int consumed;

            do
            {
                IMessage message = this.messageFactory.CreateMessage(this.buffer, out consumed);

                if (consumed == 0)
                    break;

                this.buffer = this.buffer.SubArray(consumed);

                messages.Enqueue(message);

            } while (true);        
        }

        private void OnDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            this.aggregator.Trigger(new ClientDisconnectedEvent(this));
        }

        public void Update(float delta)
        {
            IMessage message;
            while (messages.TryDequeue(out message))
            {
                logger.DebugFormat("<- CtoS packet, header: {0}", message.Header);
                message.Session = this;

                this.aggregator.Trigger(message);
            }

            UpdateProc(delta);
        }

        public virtual void UpdateProc(float delta)
        {

        }
    }
}

