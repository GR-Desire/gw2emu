﻿using System;
using GameRevision.GW2Emu.Common.Messaging;

namespace GameRevision.GW2Emu.Common.Session
{
    public interface ISession
    {
        object Attachment { get; set; }
        void Disconnect();
		bool Send(IMessage message);
        bool Send(byte[] buffer);
        void Update(float delta);
    }
}
