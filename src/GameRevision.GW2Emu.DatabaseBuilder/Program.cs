﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.Common.Database;

namespace GameRevision.GW2Emu.DatabaseBuilder
{
    class Program
    {
        static bool Query(string message)
        {
            Console.WriteLine("Would you like to build " + message + " ? (y/n)");
            return Console.ReadLine() == "y";
        }

        static void Main(string[] args)
        {
            using (LoginEntities login = new LoginEntities())
            {
                bool all = Query("all tables");
                if(all || Query("items"))
                    Items.Build("en", login);

                if (all || Query("events"))
                    Events.Build("en", login);

                Console.WriteLine("Commiting changes to database...");
                login.SaveChanges();
                Console.WriteLine("Done. Press a key to exit.");
            }
            
            Console.ReadLine();
        }
    }
}
