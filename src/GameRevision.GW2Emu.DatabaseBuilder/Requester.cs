﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.DatabaseBuilder
{
    public static class Requester
    {
        public static string Request(string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream resStream = response.GetResponseStream();
                StreamReader objReader = new StreamReader(resStream);

                return objReader.ReadToEnd();
            }
            catch
            {
            }
            return null;
        }
    }
}
