﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.Common.Database;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace GameRevision.GW2Emu.DatabaseBuilder
{
    public static class Events
    {
        public static void Build(string lang, LoginEntities logon)
        {
            Console.WriteLine("Removing old events...");

            var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)logon).ObjectContext;
            objCtx.ExecuteStoreCommand("DELETE FROM event");

            Console.WriteLine("Building Events...");
            JObject o = JObject.Parse(Requester.Request("https://api.guildwars2.com/v1/event_details.json"));

            int i = 0;
            Object iLock = new Object();

            JObject events = (JObject)o["events"];
            IList<string> keys = events.Properties().Select(p => p.Name).ToList();

            Parallel.ForEach(keys, key =>
            {
                int current = 0;
                lock (iLock)
                {
                    current = ++i;
                    ProgressBar.RenderConsoleProgress(keys.Count, current);
                }
                
                BuildEvent(events[key], Guid.Parse(key), logon);
            });

            Console.WriteLine("\nDone.");
        }

        private static void BuildEvent(JToken data, Guid id, LoginEntities logon)
        {
            lock (logon)
            {
                logon.events.Add(new @event() { 
                        id = id, 
                        name = (string)data["name"], 
                        level = (byte)data["level"],
                        map = (int)data["map_id"]
                    });
            }
            
        }
    }
}
