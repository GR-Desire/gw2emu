﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameRevision.GW2Emu.Common.Database;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace GameRevision.GW2Emu.DatabaseBuilder
{
    public static class Items
    {
        public static void Build(string lang, LoginEntities logon)
        {
            Console.WriteLine("Removing old items...");

            var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)logon).ObjectContext;
            objCtx.ExecuteStoreCommand("DELETE FROM item");

            Console.WriteLine("Building Items...");
            JObject o = JObject.Parse(Requester.Request("https://api.guildwars2.com/v1/items.json"));
            JArray ids = (JArray)o["items"];

            int i = 0;
            Object iLock = new Object();

            Parallel.ForEach(ids, id =>
            {
                int current = 0;
                lock (iLock)
                {
                    current = ++i;
                    ProgressBar.RenderConsoleProgress(ids.Count, current);
                }
                
                BuildItem(lang, (int)id, logon);
            });

            Console.WriteLine("\nDone.");
        }

        private static void BuildItem(string lang, int id, LoginEntities logon)
        {
            string r = Requester.Request("https://api.guildwars2.com/v1/item_details.json?item_id=" + id + "&lang=" + lang);
            if (r == null)
                return;
            JObject o = JObject.Parse(r);
            lock (logon)
            {
                logon.items.Add(new item() { 
                        id = id, 
                        name = (string)o["name"], 
                        description = (string)o["description"],
                        level = byte.Parse((string)o["level"]),
                        vendor_value = int.Parse((string)o["vendor_value"]),   
                        type = (string)o["type"]
                    });
            }
            
        }
    }
}
