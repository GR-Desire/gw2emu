﻿using System;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Messages.StoC;

namespace GameRevision.GW2Emu.LoginServer.Views
{
    public static class AccountView
    {
        public static void SendAccountMedals(ISession session, int syncCount, byte[] accountMedalData)
        {
            session.Send(new P10_AccountMedalsMessage()
            {
                SyncCount = syncCount,
                MedalData = accountMedalData
            });
        }

        public static void SendP27(ISession session, int syncCount)
        {
            session.Send(new P27_UnknownMessage()
            {
                SyncCount = syncCount,
                Unknown4 = new P27_UnknownMessage.Struct1[]
                {
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 0,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 1,
                        Unknown3 = 59
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 2,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 3,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 4,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 5,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 6,
                        Unknown3 = 3
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 7,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 8,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 9,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 10,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 11,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 12,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 13,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 14,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 15,
                        Unknown3 = 10
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 16,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 17,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 18,
                        Unknown3 = 4
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 19,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 20,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 21,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 22,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 23,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 24,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 25,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 26,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 27,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 28,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 29,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 30,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 31,
                        Unknown3 = 1
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 32,
                        Unknown3 = 6
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 33,
                        Unknown3 = 8
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 34,
                        Unknown3 = 2000
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 35,
                        Unknown3 = 2000
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 36,
                        Unknown3 = 2000
                    },
                    new P27_UnknownMessage.Struct1()
                    {
                        Unknown2 = 37,
                        Unknown3 = 2000
                    }
                }
            });
        }

        public static void SendAccountInfo(ISession session, int syncCount, int timestamp, int homeWorld, byte[] datEncryptionKey)
        {
            session.Send(new P08_AccountInfoMessage()
            {
                SyncCount = syncCount,
                SessionId = new Guid("db4f3095-ef2c-488d-9b53-0e7aaba420be"),
                CharacterId = new Guid("b6c69549-baef-44eb-bfe5-fea87c5d2167"),
                Unknown3 = 2,
                Unknown4 = 5,
                Timestamp = timestamp,
                HomeWorld = homeWorld,
                Unknown7 = homeWorld,
                Unknown8 = 3,
                DatEncryptionKey = datEncryptionKey,
                Unknown16 = new P08_AccountInfoMessage.Struct10[]
                {
                    new P08_AccountInfoMessage.Struct10()
                    {
                        Unknown11 = 2,
                        Unknown12 = 1,
                        Unknown13 = 0,
                        Unknown14 = 0,
                        Unknown15 = 0
                    }
                },
                Unknown17 = new byte[32]
            });
        }
    }
}
