﻿using System;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Messages.StoC;

namespace GameRevision.GW2Emu.LoginServer.Views
{
    // TODO: change class name
    public static class MiscView
    {
        public static void SendPingReply(ISession session, int timestamp)
        {
            P01_PingServerReplyMessage pingReply = new P01_PingServerReplyMessage()
            {
                Timestamp = timestamp
            };

            session.Send(pingReply);
        }

        public static void SendComputerInfoReply(ISession session)
        {
            session.Send(new P02_ComputerInfoReplyMessage()
                {
                    Unknown0 = 3797550815
                });
        }

        // TODO: find a reasonable name for this handler
        public static void SendP15(ISession session)
        {
            session.Send(new P15_UnknownMessage());
        }

        public static void SendClientSync(ISession session, int syncCount, int unk1, int unk2, int unk3, int unk4)
        {
            // TODO: make dynamic

            // needs r.e.
            session.Send(new P04_ClientSyncMessage()
            {
                SyncCount = syncCount,
                Unknown1 = unk1,
                Unknown2 = unk2,
                Unknown3 = unk3,
                Unknown4 = unk4
            });
        }

        public static void SendP16(ISession session, int syncCount, Guid sessionId)
        {
            session.Send(new P16_UnknownMessage()
            {
                SyncCount = syncCount,
                Unknown1 = sessionId,
                Unknown2 = 0,
                Unknown3 = unchecked((uint)3586078909),
                Unknown4 = 168, // related to home servers
                Unknown5 = 1800, // related to home servers, transfer cost
                Unknown6 = 0
            });
        }

        public static void SendP23(ISession session)
        {
            session.Send(new P23_UnknownMessage());
        }
    }
}
