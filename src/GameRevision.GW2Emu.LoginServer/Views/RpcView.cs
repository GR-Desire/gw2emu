﻿using GameRevision.GW2Emu.Common.Messaging.Rpc;
using GameRevision.GW2Emu.Common.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.LoginServer.Views
{
    public static class RpcView
    {
        public static void SendCreateSession(ISession session, string mail, int mode)
        {
            session.Send(
                new P0001_CreateSessionMessage()
                {
                    Email = mail,
                    Mode = mode
                });
        }
    }
}
