﻿using System;
using System.Net;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Messages.StoC;

namespace GameRevision.GW2Emu.LoginServer.Views
{
    public static class ExternalServerView
    {
        public static void SendHomeWorldInfo(ISession session, int identity, int population, int transferCost)
        {
            session.Send(new P25_HomeWorldInfoMessage()
            {
                Identity = identity,
                Population = population,
                TransferCost = transferCost,
                Unknown3 = 168
            });
        }

        public static void SendP24(ISession session)
        {
            session.Send(new P24_UnknownMessage());
        }

        public static void SendReferToGameServer(ISession session, int syncCount, int stage, int mapId, IPEndPoint connectionInfo)
        {
            session.Send(new P20_ReferToGameServerMessage()
            {
                SyncCount = syncCount,
                Stage = stage,
                Unknown2 = 0,
                MapId = mapId,
                ConnectionInfo = connectionInfo,
                Unknown5 = 0
            });
        }
    }
}
