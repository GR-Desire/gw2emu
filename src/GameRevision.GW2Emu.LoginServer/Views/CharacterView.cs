﻿using System;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Messages.StoC;

namespace GameRevision.GW2Emu.LoginServer.Views
{
    public class CharacterView
    {
        public static void SendCharacterInfo(ISession session, int syncCount, string characterName, Guid characterId, byte[] characterData)
        {
            session.Send(new P17_CharacterInfoMessage()
            {
                SyncCount = syncCount,
                CharacterName = characterName,
                CharacterId = characterId,
                CharacterData = characterData
            });
        }

    }
}
