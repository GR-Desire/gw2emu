﻿using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Messaging.Rpc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.LoginServer.Controllers
{
    public class SessionController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<ClientDisconnectedEvent>(this.OnClientDisconnected);
        }

        private void OnClientDisconnected(ClientDisconnectedEvent evt)
        {
            LoginServerApp.Instance.RemoveSession(evt.Session);
        }
    }
}
