﻿using System;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Views;
using GameRevision.GW2Emu.LoginServer.Messages.CtoS;

namespace GameRevision.GW2Emu.LoginServer.Controllers
{
    // this class is probably only temporary. split the handlers into several other controllers
    public class MiscController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<P01_PingServerMessage>(this.OnPingServer);
            aggregator.Register<P03_ComputerInfoMessage>(this.OnComputerInfo);
            aggregator.Register<P12_UnknownMessage>(this.OnP12);
            aggregator.Register<P14_UnknownMessage>(this.OnP14);
        }

        private void OnPingServer(P01_PingServerMessage message)
        {
            ISession session = message.Session;
            int timestamp = message.Timestamp;

            MiscView.SendPingReply(session, timestamp);
        }

        private void OnComputerInfo(P03_ComputerInfoMessage message)
        {
            ISession session = message.Session;

            MiscView.SendComputerInfoReply(session);
        }

        private void OnP12(P12_UnknownMessage message)
        {
            ISession session = message.Session;
            int syncCount = message.SyncCount;

            MiscView.SendClientSync(session, syncCount, 0, 4, 5, 2478);
        }

        private void OnP14(P14_UnknownMessage message)
        {
            ISession session = message.Session;
            int syncCount = message.SyncCount;

            MiscView.SendClientSync(session, syncCount, 0, 4, 5, 2923);
        }
    }
}
