﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Globalization;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Common;

using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.LoginServer.Views;
using GameRevision.GW2Emu.LoginServer.Models;
using GameRevision.GW2Emu.LoginServer.Session;
using GameRevision.GW2Emu.LoginServer.Messages.CtoS;

namespace GameRevision.GW2Emu.LoginServer.Controllers
{
    public class LoginController : IRegisterable
    {
        private byte[] datEncryptionKey;

        public LoginController(byte[] datEncryptionKey)
        {
            this.datEncryptionKey = datEncryptionKey;
        }

        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<StartupEvent>(this.OnStartup);
            aggregator.Register<P10_ClientSessionInfoMessage>(this.OnClientSessionInfo);
            aggregator.Register<P25_QueryWorldMessage>(this.OnClientQuery);
        }

        private void OnStartup(StartupEvent evt)
        {
        }

        private void OnClientQuery(P25_QueryWorldMessage message)
        {
            ISession session = message.Session;
            var attachment = (LoginSessionAttachment)session.Attachment;
            int syncCount = message.SyncCount;

            using (LoginEntities entities = new LoginEntities())
            {
                var playerAccount = (from a in entities.accounts
                                     where a.id == attachment.AccountId
                                     select a).Single();

                MiscView.SendP23(session);

                ExternalServerModel.ProcessHomeWorldInfo(session, entities);

                CharacterModel.ProcessCharacters(session, entities, syncCount, playerAccount);

                MiscView.SendClientSync(session, syncCount, 0, 6, 6, 3418);
            }
        }

        private void OnClientSessionInfo(P10_ClientSessionInfoMessage message)
        {
            try
            {
                ISession session = message.Session;

                var attachment = new LoginSessionAttachment();
                session.Attachment = attachment;

                attachment.AccountId = message.AccountId;

                int syncCount = message.SyncCount;

                using (LoginEntities entities = new LoginEntities())
                {
                    var playerAccount = (from a in entities.accounts
                                         where a.id == attachment.AccountId
                                         select a)
                                         .Include(a => a.Characters.Select(ch => ch.Items))
                                         .SingleOrDefault();

                    if (playerAccount == null)
                    {
                        session.Disconnect();
                        return;
                    }

                    attachment.Mail = playerAccount.mail;

                    MiscView.SendP15(session);
                    MiscView.SendClientSync(session, 1, 21, 6, 3, 2205);

                    CharacterModel.ProcessCharacters(session, entities, syncCount, playerAccount);

                    AccountModel.ProcessAccount(session, entities, syncCount, playerAccount, this.datEncryptionKey);

                    MiscView.SendP16(session, syncCount, new Guid("db4f3095-ef2c-488d-9b53-0e7aaba420be"));
                    MiscView.SendP23(session);

                    ExternalServerModel.ProcessHomeWorldInfo(session, entities);

                    MiscView.SendClientSync(session, syncCount, 0, 49, 7004, 823);
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
                message.Session.Disconnect();
            }
            
        }
    }
}
