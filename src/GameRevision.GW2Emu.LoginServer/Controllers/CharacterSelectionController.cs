﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Globalization;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Data.Common;

using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.LoginServer.Models;
using GameRevision.GW2Emu.LoginServer.Messages.CtoS;
using GameRevision.GW2Emu.LoginServer.Session;

namespace GameRevision.GW2Emu.LoginServer.Controllers
{
    public class CharacterSelectionController : IRegisterable
    {
        private byte[] datEncryptionKey;

        public CharacterSelectionController(byte[] datEncryptionKey)
        {
            this.datEncryptionKey = datEncryptionKey;
        }

        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<StartupEvent>(this.OnStartup);
            aggregator.Register<P22_CharacterPlayMessage>(this.OnCharacterPlay);
            aggregator.Register<P20_CharacterDeleteMessage>(this.OnCharacterDelete);
        }

        private void OnStartup(StartupEvent evt)
        {
        }

        private void OnCharacterPlay(P22_CharacterPlayMessage message)
        {
            ISession session = message.Session;
            LoginSessionAttachment attachment = session.Attachment as LoginSessionAttachment;
            int syncCount = message.SyncCount;
            using (LoginEntities entities = new LoginEntities())
            {
                var character = (from c in entities.characters
                                 where c.account == attachment.AccountId
                                 && c.name == message.CharacterName
                                 select c).SingleOrDefault();


                if (character != null)
                    attachment.CharacterId = character.id;
            }

            CharacterSelectionModel.ProcessCharacterPlay(session, syncCount, this.datEncryptionKey);
        }

        private void OnCharacterDelete(P20_CharacterDeleteMessage message)
        {
            ISession session = message.Session;
            LoginSessionAttachment attachment = session.Attachment as LoginSessionAttachment;

            using (LoginEntities entities = new LoginEntities())
            {
                var character = (from c in entities.characters
                                where c.account == attachment.AccountId
                                && c.name == message.CharacterName
                                select c).SingleOrDefault();


                if (character != null)
                    entities.characters.Remove(character);

                entities.SaveChanges();
            }
            

            CharacterSelectionModel.ProcessCharacterDelete(session, message.SyncCount);
        }
    }
}
