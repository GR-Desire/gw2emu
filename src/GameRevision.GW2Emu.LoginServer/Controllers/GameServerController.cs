﻿using System;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Models;
using GameRevision.GW2Emu.LoginServer.Messages.CtoS;
using GameRevision.GW2Emu.LoginServer.Views;
using GameRevision.GW2Emu.LoginServer.Session;
using GameRevision.GW2Emu.LoginServer.Internals;
using GameRevision.GW2Emu.Common.Database;

namespace GameRevision.GW2Emu.LoginServer.Controllers
{
    public class GameServerController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<P29_UnknownMessage>(this.OnP29);
        }

        private void OnP29(P29_UnknownMessage message)
        {
            try
            {
                ISession session = message.Session;
                LoginSessionAttachment attachment = (LoginSessionAttachment)session.Attachment;

                int syncCount = message.SyncCount;
                int stage = message.Stage;
                int mapId = message.MapId;

                using (LoginEntities entities = new LoginEntities())
                {
                    var account = entities.accounts.Find(attachment.AccountId);
                    if (account.AccountSession == null)
                    {
                        account.AccountSession = new account_session();
                        account.AccountSession.id = Guid.NewGuid();
                    }

                    account.AccountSession.map = mapId;
                    account.AccountSession.view = message.Unknow2;

                    if (attachment.CharacterId != Guid.Empty)
                        account.AccountSession.character = attachment.CharacterId;
                    else
                        account.AccountSession.character = null;

                    entities.SaveChanges();
                }

                ExternalServerModel.ProcessReferToGameServer(session, syncCount, stage, mapId);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
                message.Session.Disconnect();
            }
        }
    }
}
