﻿using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Messaging.Rpc;
using GameRevision.GW2Emu.GameServer.Session;
using GameRevision.GW2Emu.LoginServer.Internals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.LoginServer.Controllers
{
    public class RpcController : IRegisterable
    {
        public void Register(IEventAggregator aggregator)
        {
            aggregator.Register<P1001_CreateSessionStatusMessage>(this.OnCreateSessionStatus);
            aggregator.Register<P1002_RegisterServerMessage>(this.OnRegisterServer);
        }

        private void OnCreateSessionStatus(P1001_CreateSessionStatusMessage message)
        {

        }

        private void OnRegisterServer(P1002_RegisterServerMessage message)
        {
            GameServerManager.Register((RpcSession)message.Session, message.Id, message.Type);
        }
    }
}
