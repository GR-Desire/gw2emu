﻿using System;
using System.Threading;

namespace GameRevision.GW2Emu.LoginServer
{
    public static class Program
    {
        public static void Main()
        {
            try
            {
                Console.Title = "GW2Emu login server";

                log4net.Config.XmlConfigurator.Configure();

                // register the event handlers
                RpcServerApp.Instance.RegisterHandlers();
                LoginServerApp.Instance.RegisterHandlers();

                // start the server
                RpcServerApp.Instance.Start();
                LoginServerApp.Instance.Start();

                while (!Console.KeyAvailable)
                {
                    LoginServerApp.Instance.UpdateOneFrame(0.0f);
                    Thread.Sleep(50);
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadKey(true);
        }
    }
}
