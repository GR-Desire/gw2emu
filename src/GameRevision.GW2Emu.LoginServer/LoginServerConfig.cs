﻿using System;
using GameRevision.GW2Emu.Common;

namespace GameRevision.GW2Emu.LoginServer
{
    public class LoginServerConfig
    {
        private static string FileName = "GameRevision.GW2Emu.LoginServer.xml";

        public static LoginServerConfig Load()
        {
            if (!XmlConfig.Exists(FileName))
            {
                XmlConfig.Save<LoginServerConfig>(FileName, new LoginServerConfig()
                    {
                        IP = "0.0.0.0",
                        Port = 8112,
                        RpcIP = "0.0.0.0",
                        RpcPort = 15471,
                        DatEncryptionKey = new byte[] { 0xd1, 0x91, 0xf7, 0x88, 0x9f, 0xd2, 0x70, 0x76, 0xa3, 0x98, 0x40, 0x78, 0xd7, 0xc2, 0xf7, 0xfc, 0x08, 0x94, 0x6f, 0xe9, 0x1d, 0x86, 0x99, 0xd7, 0x88, 0x49, 0xb1, 0xfb, 0xc8, 0x69, 0xd3, 0x0f }
                        // Version: 23244
                    });
            }

            return XmlConfig.Load<LoginServerConfig>(FileName);
        }

        public string IP;
        public string RpcIP;
        public int RpcPort;
        public int Port;
        public byte[] DatEncryptionKey;
    }
}
