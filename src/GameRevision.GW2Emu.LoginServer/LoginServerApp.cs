﻿using System;
using System.Net;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Network;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Session;
using GameRevision.GW2Emu.LoginServer.Controllers;
using log4net;

namespace GameRevision.GW2Emu.LoginServer
{
    public class LoginServerApp
    {
        private static ILog logger = LogManager.GetLogger(typeof(LoginServerApp));

        private IEventAggregator eventAggregator;
        private ClientListener clientListener;
        private SessionManager sessionManager;

        private static LoginServerApp instance;
        public static LoginServerApp Instance 
        { 
            get 
            {
                if (instance == null)
                    instance = new LoginServerApp();
                return instance; 
            } 
        }
        public LoginServerConfig config;

        private LoginServerApp()
        {
            this.eventAggregator = new EventAggregator();
            this.sessionManager = new SessionManager();

            this.config = LoginServerConfig.Load();

            IPAddress address;

            if (!IPAddress.TryParse(config.IP, out address))
            {
                address = IPAddress.Any;

                logger.Error("Couldn't parse IP address from config. Running on default.");
            }

            this.clientListener = new ClientListener(address, config.Port);
            this.clientListener.ClientConnected += OnClientConnected;

            logger.InfoFormat("Server created on {0}:{1}.", this.clientListener.EndPoint.Address, this.clientListener.EndPoint.Port);
        }

        public void RegisterHandlers()
        {
            this.eventAggregator.Register(new MiscController());
            this.eventAggregator.Register(new GameServerController());
            this.eventAggregator.Register(new LoginController(config.DatEncryptionKey));
            this.eventAggregator.Register(new CharacterSelectionController(config.DatEncryptionKey));

            this.eventAggregator.Trigger(new StartupEvent());

            logger.Info("Registered handlers.");
        }

        public void Start()
        {
            this.clientListener.Listen();

            logger.Info("Server started.");
        }

        public void Stop()
        {
            this.clientListener.Stop();

            logger.Info("Server stopped.");
        }

        public void UpdateOneFrame(float delta)
        {
            this.sessionManager.Flush();
            this.sessionManager.Update(delta);
        }

        private void OnClientConnected(object sender, ClientConnectedEventArgs e)
        {
            GenericSession session = new LoginSession(e.Client, this.eventAggregator);
            this.sessionManager.Add(session);
            session.Start();
        }

        public void RemoveSession(ISession session)
        {
            this.sessionManager.Remove(session);
        }
    }
}
