﻿using System;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Database;

namespace GameRevision.GW2Emu.LoginServer.Session
{
    public class LoginSessionAttachment
    {
        public Guid AccountId;
        public Guid CharacterId;
        public string Mail;
    }
}
