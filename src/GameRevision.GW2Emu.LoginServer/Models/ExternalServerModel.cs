﻿using System;
using System.Net;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Views;
using GameRevision.GW2Emu.Common.Database;

namespace GameRevision.GW2Emu.LoginServer.Models
{
    public static class ExternalServerModel
    {
        public static void ProcessHomeWorldInfo(ISession session, LoginEntities entities)
        {
            foreach(var s in entities.servers)
                ExternalServerView.SendHomeWorldInfo(session, s.id, (int)s.population, (int)s.transfer_cost);

            // finalize the home server phase

            ExternalServerView.SendP24(session);
        }

        public static void ProcessReferToGameServer(ISession session, int syncCount, int stage, int mapId)
        {
            // TODO: find the right game server that matches the client's current home server and the requested mapId



            IPEndPoint connectionInfo = new IPEndPoint(IPAddress.Loopback, 0);

            ExternalServerView.SendReferToGameServer(session, syncCount, stage, mapId, connectionInfo);
        }
    }
}
