﻿using System;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Database;
using GameRevision.GW2Emu.LoginServer.Views;

namespace GameRevision.GW2Emu.LoginServer.Models
{
    public static class AccountModel
    {
        public static void ProcessAccount(ISession session, LoginEntities entities, int syncCount, account account, byte[] datEncryptionKey)
        {
            // uncomment this later:
            // AccountEntity account = AccountEntity.FindById(databaseConnection, accountId);

            // int syncCount = 0; // retrieve this value from a sync count manager

            int homeWorld = 1001;
            int timestamp = 0;
            byte[] accountMedalData = new byte[] { 0x50, 0x46, 0x01, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x74, 0x63, 0x63, 0x61, 0x63, 0x6f, 0x72, 0x65, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a, 0x58, 0x00, 0x00, 0x75, 0x78, 0x61, 0x63, 0x2c, 0x00, 0x00, 0x00, 0x03, 0x00, 0x10, 0x00, 0x1a, 0x00, 0x00, 0x00, 0x06, 0xe5, 0x62, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x7f, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x6c, 0x72, 0x73, 0x40, 0x00, 0x00, 0x00, 0x02, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x9f, 0x01, 0x00, 0x00, 0x00, 0x57, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfb, 0x8b, 0xc5, 0x17, 0x00, 0xfb, 0xba, 0xd2, 0x17, 0x00, 0x00, 0x00, 0x29, 0x00, 0x00, 0x00, 0x2b, 0x00, 0x00, 0x00, 0x2a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x67, 0x75, 0x73, 0x75, 0x18, 0x00, 0x00, 0x00, 0x01, 0x00, 0x10, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x69, 0x6e, 0x70, 0x74, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x63, 0x68, 0x61, 0x74, 0x88, 0x00, 0x00, 0x00, 0x01, 0x00, 0x10, 0x00, 0x68, 0x00, 0x00, 0x00, 0x09, 0x00, 0x01, 0x03, 0x04, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x90, 0x01, 0xc8, 0x00, 0x04, 0xdd, 0x0f, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x01, 0x07, 0x02, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x01, 0x06, 0x00, 0x00, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x01, 0x06, 0x40, 0x00, 0x00, 0x00, 0x29, 0x00, 0x00, 0x00, 0x01, 0x4d, 0x00, 0x61, 0x00, 0x69, 0x00, 0x6e, 0x00, 0x00, 0x00, 0x43, 0x00, 0x6f, 0x00, 0x6d, 0x00, 0x62, 0x00, 0x61, 0x00, 0x74, 0x00, 0x00, 0x00, 0x41, 0x00, 0x75, 0x00, 0x74, 0x00, 0x72, 0x00, 0x65, 0x00, 0x00, 0x00, 0x4d, 0x00, 0x6f, 0x00, 0x6e, 0x00, 0x64, 0x00, 0x65, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x15, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x29, 0x00, 0x00, 0x00, 0x33, 0x00, 0x00, 0x00, 0x75, 0x6d, 0x61, 0x70, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x02, 0x4c, 0x01, 0xaa, 0x00, 0x00, 0x64, 0x6c, 0x6f, 0x67, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x62, 0x00, 0x00, 0x00, 0x0a, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x24, 0xfe, 0xdc, 0x01, 0x6e, 0x01, 0x92, 0xfe, 0x12, 0x89, 0x00, 0x11, 0x02, 0x82, 0x02, 0x25, 0x00, 0x08, 0x00, 0xfe, 0xe0, 0x01, 0x14, 0x00, 0xf0, 0x02, 0x00, 0x10, 0xfe, 0xf0, 0x01, 0x6e, 0x01, 0x92, 0xfe, 0x00, 0x40, 0xff, 0xc0, 0x00, 0xcf, 0x00, 0x31, 0xff, 0x04, 0x36, 0x01, 0x64, 0x00, 0x2f, 0x00, 0xd1, 0xff, 0x00, 0x84, 0xfe, 0x7c, 0x01, 0x3c, 0x01, 0xc4, 0xfe, 0x00, 0xe2, 0xfe, 0x1e, 0x01, 0x4b, 0x01, 0xb5, 0xfe, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00};

            AccountView.SendAccountMedals(session, syncCount, accountMedalData);
            AccountView.SendP27(session, syncCount);
            AccountView.SendAccountInfo(session, syncCount, timestamp, homeWorld, datEncryptionKey);
        }
    }
}
