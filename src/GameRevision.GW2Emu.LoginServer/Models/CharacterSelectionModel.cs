﻿using System;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Views;

namespace GameRevision.GW2Emu.LoginServer.Models
{
    public static class CharacterSelectionModel
    {
        public static void ProcessCharacterPlay(ISession session, int syncCount, byte[] datEncryptionKey)
        {
            // TODO: get the timestamp and home server values from another place

            int timestamp = 0;
            int homeWorld = 1001;

            AccountView.SendAccountInfo(session, syncCount, timestamp, homeWorld, datEncryptionKey);

            MiscView.SendClientSync(session, syncCount, 0, 1002, 3, 3426);
        }

        public static void ProcessCharacterDelete(ISession session, int syncCount)
        {
            MiscView.SendClientSync(session, syncCount, 0, 4, 18, 258);
        }
    }
}
