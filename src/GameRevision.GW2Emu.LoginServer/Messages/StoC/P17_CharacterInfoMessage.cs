/*
 * This code was autogenerated by
 * GameRevision.GW2Emu.CodeWriter.
 * Date generated: 24-07-13
 */

using System;
using System.IO;
using System.Net;
using GameRevision.GW2Emu.Common;
using GameRevision.GW2Emu.Common.Math;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.Common.Messaging;
using GameRevision.GW2Emu.Common.Serialization;

namespace GameRevision.GW2Emu.LoginServer.Messages.StoC
{
    public class P17_CharacterInfoMessage : GenericMessage
    {
        public int SyncCount;
        public Guid CharacterId;
        public int Unknown2;
        public string CharacterName;
        public byte[] CharacterData;
        
        public override ushort Header
        {
            get
            {
                return 17;
            }
        }
        
        public override void Serialize(Serializer serializer)
        {
            serializer.Write(this.Header);
            serializer.WriteVarint(this.SyncCount);
            serializer.Write(this.CharacterId);
            serializer.WriteVarint(this.Unknown2);
            serializer.WriteUtf16String(this.CharacterName);
            serializer.Write((ushort)CharacterData.Length);
            for (int i = 0; i < CharacterData.Length; i++)
            {
                serializer.Write(CharacterData[i]);
            }
        }
    }
}
