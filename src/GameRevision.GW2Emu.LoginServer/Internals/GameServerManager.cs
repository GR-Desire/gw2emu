﻿using GameRevision.GW2Emu.GameServer.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.LoginServer.Internals
{
    public class GameServerManager
    {
        private static GameServerManager instance = new GameServerManager();
        private Dictionary<int, GameServer> servers = new Dictionary<int, GameServer>(); 

        private GameServerManager()
        {

        }

        public static void Register(RpcSession session, int id, int type)
        {
            if(instance.servers.ContainsKey(id))
                instance.servers.Remove(id);

            instance.servers.Add(id, 
                new GameServer() 
                {
                    Id = id,
                    Type = type,
                    Session = session
                });
        }

        public static GameServer FindById(int id)
        {
            return instance.servers[id];
        }
    }
}
