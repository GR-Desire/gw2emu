﻿using GameRevision.GW2Emu.GameServer.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameRevision.GW2Emu.LoginServer.Internals
{
    public class GameServer
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public RpcSession Session { get; set; }
    }
}
