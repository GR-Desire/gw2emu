﻿using System;
using System.Net;
using GameRevision.GW2Emu.Common.Events;
using GameRevision.GW2Emu.Common.Network;
using GameRevision.GW2Emu.Common.Session;
using GameRevision.GW2Emu.LoginServer.Session;
using GameRevision.GW2Emu.LoginServer.Controllers;
using GameRevision.GW2Emu.GameServer.Session;
using log4net;

namespace GameRevision.GW2Emu.LoginServer
{
    public class RpcServerApp
    {
        private static ILog logger = LogManager.GetLogger(typeof(RpcServerApp));

        private IEventAggregator eventAggregator;
        private ClientListener clientListener;

        private static RpcServerApp instance;
        public static RpcServerApp Instance
        {
            get
            {
                if (instance == null)
                    instance = new RpcServerApp(LoginServerApp.Instance.config);
                return instance;
            }
        }

        private RpcServerApp(LoginServerConfig config)
        {
            this.eventAggregator = new EventAggregator();

            IPAddress address;

            if (!IPAddress.TryParse(config.RpcIP, out address))
            {
                address = IPAddress.Any;

                logger.Error("Couldn't parse IP address from config. Running on default.");
            }

            this.clientListener = new ClientListener(address, config.RpcPort);
            this.clientListener.ClientConnected += OnClientConnected;

            logger.InfoFormat("RPC Server created on {0}:{1}.", this.clientListener.EndPoint.Address, this.clientListener.EndPoint.Port);
        }

        public void RegisterHandlers()
        {
            this.eventAggregator.Register(new RpcController());

            this.eventAggregator.Trigger(new StartupEvent());

            logger.Info("Registered RPC handlers.");
        }

        public void Start()
        {
            this.clientListener.Listen();

            logger.Info("RPC Server started.");
        }

        public void Stop()
        {
            this.clientListener.Stop();

            logger.Info("RPC Server stopped.");
        }

        private void OnClientConnected(object sender, ClientConnectedEventArgs e)
        {
            RpcSession session = new RpcSession(e.Client, this.eventAggregator);
            session.Start();
        }
    }
}
