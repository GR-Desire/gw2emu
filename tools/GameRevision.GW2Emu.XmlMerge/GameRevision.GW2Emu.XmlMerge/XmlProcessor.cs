﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace GameRevision.GW2Emu.XmlMerge
{
    public class XmlProcessor
    {
        private Templates oldTemplates;
        private Templates newTemplates;
        private Dictionary<ProtocolSimpleTypes, IList<PacketType>> oldTemplatesPackets;
        private Dictionary<ProtocolSimpleTypes, IList<PacketType>> newTemplatesPackets;

        public XmlProcessor(string oldTemplatesFile, string newTemplatesFile)
        {
            this.oldTemplatesPackets = new Dictionary<ProtocolSimpleTypes, IList<PacketType>>();
            this.newTemplatesPackets = new Dictionary<ProtocolSimpleTypes, IList<PacketType>>();

            using (TextReader reader = new StreamReader(oldTemplatesFile))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Templates));
                this.oldTemplates = (Templates)serializer.Deserialize(reader);
            }

            using (TextReader reader = new StreamReader(newTemplatesFile))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Templates));
                this.newTemplates = (Templates)serializer.Deserialize(reader);
            }
        }

        public void InitializeHeaders()
        {
            foreach (CommunicationDirection protocol in this.oldTemplates.Protocol)
            {
                IList<PacketType> packets = new List<PacketType>();

                int next = 0;
                int lastHeader = int.Parse(protocol.Packet[protocol.Packet.Length - 1].header);

                for (int i = 0; next < lastHeader; i++)
                {
                    PacketType packet = protocol.Packet[next];

                    if (short.Parse(packet.header) == (i + 1))
                    {
                        packets.Add(packet);
                        next++;
                    }
                    else
                    {
                        packets.Add(null);
                    }

                    if (next == protocol.Packet.Length)
                    {
                        break;
                    }
                }

                this.oldTemplatesPackets.Add(protocol.type, packets);
            }

            foreach (CommunicationDirection protocol in this.newTemplates.Protocol)
            {
                IList<PacketType> packets = new List<PacketType>();

                int next = 0;
                int lastHeader = int.Parse(protocol.Packet[protocol.Packet.Length - 1].header);

                for (int i = 0; next < lastHeader; i++)
                {
                    PacketType packet = protocol.Packet[next];

                    if (short.Parse(packet.header) == (i + 1))
                    {
                        packets.Add(packet);
                        next++;
                    }
                    else
                    {
                        packets.Add(null);
                    }

                    if (next == protocol.Packet.Length)
                    {
                        break;
                    }
                }

                this.newTemplatesPackets.Add(protocol.type, packets);
            }
        }

        public void Run(string mergedXmlPath)
        {
            if (this.oldTemplates.Protocol.Length != this.newTemplates.Protocol.Length)
            {
                throw new InvalidOperationException();
            }

            List<CommunicationDirection> mergedProtocols = new List<CommunicationDirection>();

            Parallel.ForEach(this.oldTemplates.Protocol, delegate(CommunicationDirection protocol)
            {
                List<PacketType> mergedTemplatesList = new List<PacketType>();
                IList<PacketType> oldTemplatesList = this.oldTemplatesPackets[protocol.type];
                IList<PacketType> newTemplatesList = this.newTemplatesPackets[protocol.type];

                int count = Math.Max(oldTemplatesList.Count, newTemplatesList.Count);

                for (int i = 0; i < count; i++)
                {
                    PacketType oldTemplate = null;
                    PacketType newTemplate = null;

                    if (oldTemplatesList.Count > i)
                    {
                        oldTemplate = oldTemplatesList[i];
                    }

                    if (newTemplatesList.Count > i)
                    {
                        newTemplate = newTemplatesList[i];
                    }

                    if (oldTemplate == null && newTemplate == null)
                    {
                        // no templates exists with the current header (i)
                    }
                    else if (oldTemplate == null && newTemplate != null)
                    {
                        // add template

                        mergedTemplatesList.Add(newTemplate);
                    }
                    else if (oldTemplate != null && newTemplate == null)
                    {
                        // remove template

                        Console.WriteLine("Removed: {0} {1}", protocol.type, oldTemplate.header);
                    }
                    else
                    {
                        if (oldTemplate.Field != null || newTemplate.Field != null)
                        {
                            if (oldTemplate.Field != null && newTemplate.Field == null || oldTemplate.Field == null && newTemplate.Field != null || oldTemplate.Field.Length != newTemplate.Field.Length)
                            {
                                Console.WriteLine("Changed: {0} {1}", protocol.type, oldTemplate.header);

                                mergedTemplatesList.Add(newTemplate);
                            }
                            else
                            {
                                for (int j = 0; i < oldTemplate.Field.Length; i++)
                                {
                                    BasicFieldType oldField = oldTemplate.Field[j];
                                    BasicFieldType newField = newTemplate.Field[j];

                                    if (oldField.Info != newField.Info || oldField.@static != newField.@static || oldField.staticSpecified != newField.staticSpecified || oldField.type != newField.type)
                                    {
                                        Console.WriteLine("Changed: {0} {1}", protocol.type, oldTemplate.header);

                                        mergedTemplatesList.Add(newTemplate);
                                        break;
                                    }
                                }
                            }
                        }

                        // iterate fields:

                        // ---------------

                        // done iterating

                        // ---------------

                        // if changed:

                        // else:
                    }
                }

                CommunicationDirection mergedProtocol = new CommunicationDirection();
                mergedProtocol.type = protocol.type;
                mergedProtocol.Packet = mergedTemplatesList.ToArray();

                mergedProtocols.Add(mergedProtocol);
            });

            Templates mergedTemplates = new Templates();
            mergedTemplates.Protocol = mergedProtocols.ToArray();

            this.WriteXml(mergedTemplates, mergedXmlPath);
        }

        private void WriteXml(Templates templates, string path)
        {
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Templates));
                serializer.Serialize(writer, templates);
            }
        }
    }
}
