﻿using System;
using System.IO;
using System.Reflection;

namespace GameRevision.GW2Emu.XmlMerge
{
    public static class Program
    {
        private const string OldTemplatesFile = @"Xml\OldTemplates.xml";
        private const string NewTemplatesFile = @"Xml\NewTemplates.xml";
        private const string MergedTemplatesFile = @"Xml\MergedTemplates.xml";

        public static void Main(string[] args)
        {
            XmlProcessor processor = new XmlProcessor(OldTemplatesFile, NewTemplatesFile);
            processor.InitializeHeaders();
            processor.Run(MergedTemplatesFile);

            Console.WriteLine();
            Console.WriteLine("Done!");
            Console.ReadKey(true);
        }
    }
}
