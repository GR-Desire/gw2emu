CREATE USER 'logon'@'localhost' IDENTIFIED BY 'logon';

GRANT ALL PRIVILEGES
ON logon.*
TO 'logon'@'localhost';

FLUSH PRIVILEGES;