ALTER TABLE `logon`.`character` DROP COLUMN `main_hand`, DROP COLUMN `off_hand`, DROP COLUMN `chest`, DROP COLUMN `feet`, DROP COLUMN `unkItem1`, DROP COLUMN `hands`, DROP COLUMN `legs`, DROP COLUMN `shoulders`, DROP COLUMN `mask`, DROP INDEX `FK_character_main_hand`, DROP INDEX `FK_character_off_hand`, DROP INDEX `FK_character_chest`, DROP INDEX `FK_character_feet`, DROP INDEX `FK_character_unkitem1`, DROP INDEX `FK_character_hands`, DROP INDEX `FK_character_legs`, DROP INDEX `FK_character_shoulders`, DROP INDEX `FK_character_mask`, DROP FOREIGN KEY `FK_character_chest`, DROP FOREIGN KEY `FK_character_feet`, DROP FOREIGN KEY `FK_character_hands`, DROP FOREIGN KEY `FK_character_legs`, DROP FOREIGN KEY `FK_character_main_hand`, DROP FOREIGN KEY `FK_character_mask`, DROP FOREIGN KEY `FK_character_off_hand`, DROP FOREIGN KEY `FK_character_shoulders`, DROP FOREIGN KEY `FK_character_unkitem1`; 

ALTER TABLE `logon`.`character_item` ADD COLUMN `type` TINYINT UNSIGNED DEFAULT 4 NULL AFTER `color4`, ADD COLUMN `position` TINYINT UNSIGNED NULL AFTER `type`; 

ALTER TABLE `logon`.`character` CHANGE `id` `id` CHAR(36) NOT NULL, CHANGE `account` `account` CHAR(36) NULL; 

ALTER TABLE `logon`.`character` DROP COLUMN `account`, CHANGE `id` `id` CHAR(36) NOT NULL, DROP INDEX `FK_character_account`, DROP FOREIGN KEY `FK_character_account`; 

ALTER TABLE `logon`.`account` CHANGE `id` `id` CHAR(36) NOT NULL; 

ALTER TABLE `logon`.`character` ADD COLUMN `account` CHAR(36) NULL AFTER `id`, ADD CONSTRAINT `FK_character_account` FOREIGN KEY (`account`) REFERENCES `logon`.`account`(`id`); 

ALTER TABLE `logon`.`character_item` ADD COLUMN `character` CHAR(36) NULL AFTER `id`; 

ALTER TABLE `logon`.`character_item` ADD CONSTRAINT `FK_character_item_character` FOREIGN KEY (`character`) REFERENCES `logon`.`character`(`id`); 

ALTER TABLE `logon`.`character_item` CHANGE `type` `type` TINYINT(3) UNSIGNED DEFAULT 4 NOT NULL, CHANGE `position` `position` TINYINT(3) UNSIGNED NOT NULL, ADD COLUMN `count` INT UNSIGNED DEFAULT 0 NOT NULL AFTER `position`; 

ALTER TABLE `logon`.`character_item` ADD COLUMN `specific` MEDIUMBLOB NULL AFTER `count`; 
