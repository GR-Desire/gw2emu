/*
SQLyog Community v11.12 (64 bit)
MySQL - 5.6.12 : Database - logon
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`logon` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `logon`;

/*Data for the table `character` */

insert  into `character`(`id`,`account`,`name`,`unknown0`,`mapId`,`mapCompletion`,`level`,`class`,`appearance`,`unknown1`,`unknown2`) values ('29c2cada-ba56-4d4c-9e4e-bbac8caddcd6','c9b14d93-a93b-45dc-8fd5-b1c2919dae4b','test',0,29,0,80,1,NULL,0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
