/*
SQLyog Community v11.12 (64 bit)
MySQL - 5.6.12 : Database - logon
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`logon` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `logon`;

/*Data for the table `character_item` */

insert  into `character_item`(`id`,`character`,`item`,`flags`,`color1`,`color2`,`color3`,`color4`,`type`,`position`,`count`,`specific`) values (1,'29c2cada-ba56-4d4c-9e4e-bbac8caddcd6',36517,0,0,0,0,0,3,1,3,NULL),(2,'29c2cada-ba56-4d4c-9e4e-bbac8caddcd6',4946,0,0,0,0,0,2,2,1,NULL),(3,'29c2cada-ba56-4d4c-9e4e-bbac8caddcd6',32139,0,0,0,0,0,2,30,1,NULL),(4,'29c2cada-ba56-4d4c-9e4e-bbac8caddcd6',8932,0,0,0,0,0,5,0,1,NULL),(6,'29c2cada-ba56-4d4c-9e4e-bbac8caddcd6',23292,0,0,0,0,0,2,19,1,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
